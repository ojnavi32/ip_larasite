<!DOCTYPE html>
<html>
<head>
	<?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
	<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Contact Us</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid gmaps">
		<div class="row">
			<iframe
			  width="100%"
			  height="450"
			  frameborder="0" style="border:0"
			  src="https://www.google.com/maps/embed/v1/place?key= AIzaSyBIkanccQKfbnbqJHVG_tXI21zFYTRMf9I 
			    &q=JFH Horticultural Supplies Ltd+Lodge Rd+Sandbach+CW11" allowfullscreen>
			</iframe>
		</div>
	</div>
	<div class="container contact-form">
		<div class="row">
			<div class="col-md-3">
				<div class="contact-icon col-centered">
					<i class="fa fa-home fa-3x" aria-hidden="true"></i>
				</div>
				<div class="contact-elements">
					<h2>Address</h2>
					<p>Unit 2, Lower Complex, Lodge Road, Sandbach, Cheshire CW11 3HP</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="contact-icon col-centered">
					<i class="fa fa-envelope-o fa-3x" aria-hidden="true"></i>
				</div>
				<div class="contact-elements">
					<h2>Email</h2>
					<p>mail@jfhhorticultural.com</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="contact-icon col-centered">
					<i class="fa fa-phone fa-3x" aria-hidden="true"></i>
				</div>
				<div class="contact-elements">
					<h2>Telephone</h2>
					<p>01270 212 726</p>
				</div>
			</div>
			<div class="col-md-3">
				<div class="contact-icon col-centered">
					<i class="fa fa-clock-o fa-3x" aria-hidden="true"></i>
				</div>
				<div class="contact-elements">
					<h2>Opening Hours</h2>
					<p>Monday-Friday: 8am to 5pm<br/>Saturday: Closed<br/>Sunday: Closed</p>
				</div>
			</div>
		</div>
		
		<div class="form">
			<?php if(session('Ok')): ?>
	            <div class="alert alert-success">
	                <?php echo e(session('Ok')); ?>

	            </div>
	        <?php endif; ?>
			<?php echo Form::open(array('route' => 'contact-store')); ?>

			<div class="col-md-6">
				<label>Name</label>
				<input type="text" name="name" placeholder="Name" required>
			</div>
			<div class="col-md-6">
				<label>Email</label>
				<input type="text" name="email" placeholder="Email Address" required>
			</div>
			<div class="col-md-6">
				<label>Telephone Number</label>
				<input type="text" name="number" placeholder="Telephone Number" required>
			</div>
			<div class="col-md-6">
				<label>Subject</label>
				<input type="text" name="subject" placeholder="Subject" required>
			</div>
			<div class="col-md-12">
				<label>Message</label><br/>
				<textarea name="message" required></textarea>
			</div>
			<div class="col-md-12">
				<button>Send Message<i class="fa fa-arrow-right" aria-hidden="true"></i></button>
			</div>
			<?php echo Form::close(); ?>

		</div>
	</div>

		





	<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>
