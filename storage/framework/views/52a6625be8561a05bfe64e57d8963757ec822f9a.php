<div class="container-fluid top-background">
	<div class="container-fluid top-white">
		<div class="container">
			<div class="topbar">
				<div class="col-xs-12 col-md-5 social-links pull-left">
					<span class="call-back">Call Back</span> <span class="phone"><i class="fa fa-phone" aria-hidden="true"></i></span><span class="call-back"><a href="tel:01270 212 726" onclick="ga('send', 'event', 'Mobile Click To Call', 'Call', 'Call Campaign');">01270 212 726</a></span>
					<span class="social"><a href="https://www.facebook.com/JFH-Horticultural-Supplies-Ltd-317365915270930/?hc_ref=SEARCH&fref=nf" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a><a href="https://twitter.com/JFHSandbach" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a><a href="https://plus.google.com/104803569020955114468" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a><a href="https://www.linkedin.com/company/8239356?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A8239356%2Cidx%3A2-3-4%2CtarId%3A1472830244968%2Ctas%3AJFH" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					</span></p>
				</div>
				<div class="col-md-6 pull-right">
					<ul class="top-nav">
						<li>
							<?php echo Form::open(array('url' => '/search', 'class' => 'top-form')); ?>

							<input type="text" name="search" placeholder="Search" class="header-search-input" /><button class="header-search"><i class="fa fa-search" aria-hidden="true"></i></button>
							<?php echo Form::close(); ?>

						</li>
						<?php if(JFH\Helpers::IsUserLoggedIn()): ?>
							<li><a href="/customer/logout">Logout</a></li>
						<?php else: ?>
							<li><a href="/customer">Login / Register</a></li>
						<?php endif; ?>
						<?php if(JFH\Helpers::IsUserLoggedIn()): ?>
							<li><a href="/customer/home"><?php echo JFH\Helpers::GetName(); ?>, your account</a></li>
						<?php else: ?>
							<li><a href="/customer">My Account</a></li>
						<?php endif; ?>
						<li><a href="/cart">My Cart <i class="fa fa-shopping-cart" aria-hidden="true"></i> (<?php echo e(Cart::content()->count()); ?>)</a></li>
					</ul>
				</div>
			</div>
			<div style="clear: both;"></div>
			<div class="main-nav">
				<div class="col-xs-12 col-md-5">
					<a href="/"><img src="/images/assets/logo.png" class="img-responsive"></a>
				</div>
				<div class="col-xs-12 col-md-7 pull-right">
					<nav id="nav-main">
						<ul class="top-nav-main">
							<li><a href="/">Home</a></li>
							<li><a href="http://publizr.com/jfhhorticultural/jfh-catalogue-2016">Catalogue</a></li>
							<li><a href="/product/categories">Online Ordering</a></li>
							<li><a href="/testimonials">Testimonials</a></li>
							<li><a href="/blog">Blog</a></li>
							<li><a href="/contact-us">Contact Us</a></li>
						</ul>
					</nav>
					<div id="nav-trigger">
					    <span>Menu</span>
					</div>
					<nav id="nav-mobile"></nav>
				</div>
			</div>
		</div>
	</div>