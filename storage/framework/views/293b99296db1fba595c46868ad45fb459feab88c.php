<!DOCTYPE html>
<html>
<head>
	<?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
	<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Our Blog</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>

	<div class="container blog">
		
		<?php foreach($posts as $post): ?>
			<div class="col-md-12 post-single blog-full">
				<div class="col-md-6 pull-left">
					<h1><?php echo e($post->title); ?></h1>
				</div>
				<div class="col-md-12">
					<div class="blog-featured-image">
						<img src="/images/blog/<?php echo e($post->filename); ?>" class="img-responsive">
					</div>
				</div>
				<div class="col-md-12 blog-date-posted">
					<p>Date Posted:<?php echo e(date('d-m-Y', strtotime($post->updated_at))); ?>

				</div>
				<div class="col-md-12 blog-article">
					<?php echo $post->content; ?>

				</div>
			</div>
		<?php endforeach; ?>
		
	</div>

	<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>