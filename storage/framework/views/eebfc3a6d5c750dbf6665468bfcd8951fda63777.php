<div class="container-fluid top-background">
	<div class="container-fluid top-white">
		<div class="container">
			<div class="topbar">
				<div class="col-xs-12 col-md-5 social-links pull-left">
					<span class="call-back">Call Back</span> <span class="phone"><i class="fa fa-phone" aria-hidden="true"></i></span><span class="call-back"><a href="tel:01270 212 726" onclick="ga('send', 'event', 'Mobile Click To Call', 'Call', 'Call Campaign');">01270 212 726</a></span>
					<span class="social"><a href="https://www.facebook.com/JFH-Horticultural-Supplies-Ltd-317365915270930/?hc_ref=SEARCH&fref=nf" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a><a href="https://twitter.com/JFHSandbach" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a><a href="https://plus.google.com/104803569020955114468" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a><a href="https://www.linkedin.com/company/8239356?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A8239356%2Cidx%3A2-3-4%2CtarId%3A1472830244968%2Ctas%3AJFH" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					</span></p>
				</div>
				<div class="col-xs-12 col-md-6 pull-right">
					<ul class="top-nav">
						<li>
							<?php echo Form::open(array('url' => '/search', 'class' => 'top-form')); ?>

							<input type="text" name="search" placeholder="Search" class="header-search-input" /><button class="header-search"><i class="fa fa-search" aria-hidden="true"></i></button>
							<?php echo Form::close(); ?>

						</li>
						<?php if(JFH\Helpers::IsUserLoggedIn()): ?>
							<li><a href="/customer/logout">Logout</a></li>
						<?php else: ?>
							<li><a href="/customer">Login / Register</a></li>
						<?php endif; ?>
						<?php if(JFH\Helpers::IsUserLoggedIn()): ?>
							<li><a href="/customer/home"><?php echo JFH\Helpers::GetName(); ?>, your account</a></li>
						<?php else: ?>
							<li><a href="/customer">My Account</a></li>
						<?php endif; ?>
						<li><a href="/cart">My Cart <i class="fa fa-shopping-cart" aria-hidden="true"></i> (<?php echo e(Cart::content()->count()); ?>)</a></li>
					</ul>
				</div>
			</div>
			<div style="clear: both;"></div>
			<div class="main-nav">
				<div class="col-xs-12 col-md-5">
					<a href="/"><img src="/images/assets/logo.png" class="img-responsive"></a>
				</div>
				<div class="col-xs-12 col-md-7 pull-right">
					<nav id="nav-main">
						<ul class="top-nav-main">
							<li><a href="/">Home</a></li>
							<li><a href="http://publizr.com/jfhhorticultural/jfh-catalogue-2016">Catalogue</a></li>
							<li><a href="/product/categories">Online Ordering</a></li>
							<li><a href="/testimonials">Testimonials</a></li>
							<li><a href="/blog">Blog</a></li>
							<li><a href="/contact-us">Contact Us</a></li>
						</ul>
					</nav>
					<div id="nav-trigger">
					    <span>Menu</span>
					</div>
					<nav id="nav-mobile"></nav>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid top-green">
		<div class="container">
			<div class="home-intro">
				<div class="col-xs-12 col-md-6">
					<h1>JFH Horticultural Supplies</h1>
					<p>Established over forty years ago, JFH Horticultural Supplies are a dedicated organisation committed to integrity, honesty and excellent service, specialising in moving your business forward with innovative tailored products to suit your requirements and the ever demanding needs in horticulture.</p>
					<p>We focus on service and personal contact with all our customers whatever size and location across the United Kingdom.  Our success is through the building of lasting working relationships with our clients.  Our extensive range includes products from many of the leading horticultural manufacturers.</p>
					<a href="/product/categories"><button class="online-order">Order Online</button></a>
				</div>
				<div class="col-xs-12 col-md-6">
					<script>
					var video = document.getElementById('video');
						video.addEventListener('click',function(){
				    	video.play();
				    	ga('send', 'event', 'Videos', 'play', 'Video');
					},false);
					</script>
					<video width="100%" id="video" class="embed-responsive-item" controls poster="/images/videos/placeholder.jpg" onclick="this.play();">
					  <source src="/images/videos/jfh-2016.mp4" type="video/mp4">
					  <source src="/images/videos/jfh-2016.webm" type="video/webm">
					</video> 
				</div>
				<div style="clear: both;"></div>
			</div>		
		</div>
	</div>

	<div class="container-fluid home-login">
		<div class="container">
			<div class="col-xs-12 no-padding col-md-4 pull-left">
				<h1>Login/Register</h1>
				<p>Already have a login? Great sign in. If not, register today and you'll have an online account within 24 hours.</p>
			</div>
			<div class="col-xs-12 no-padding col-md-6 pull-right">
				<?php echo Form::open(array('route' => 'customer-register-quick')); ?>

					<div class="form-group col-md-6">
						<input type="text" name="name" id="name" class="form-control" placeholder="Full Name" required>
					</div>
					<div class="form-group col-md-6">
						<input type="email" name="emailAddress" class="form-control" id="email" placeholder="Your Email Address" required>
					</div>
					<div class="form-group col-md-8">
						<input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
					</div>
					<div class="form-group col-md-4">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				<?php echo Form::close(); ?>

			</div>
		</div>
	</div>
</div>