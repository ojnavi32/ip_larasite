<!DOCTYPE html>
<html>
<head>
	<?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
	<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Our Products</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container product search">
		<div class="row">
			<div class="col-md-5 searchbox">
				<?php echo Form::open(array('url' => '/search')); ?>

					<input type="text" name="search" placeholder="Search"><button><i class="fa fa-search" aria-hidden="true"></i></button>
				<?php echo Form::close(); ?>

			</div>
		</div>
		
		<?php if(session('Added')): ?>
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-success">
	                <?php echo e(session('Added')); ?>

	            </div>
	        </div>
		</div>
		<?php endif; ?>

		<?php if(session('Comparison')): ?>
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-success">
	                Product has been added for comparison, you can view the table <a href="/comparison-view/">here</a>
	            </div>
	        </div>
		</div>
		<?php endif; ?>

		<?php if(session('Error')): ?>
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-warning">
	                <?php echo e(session('Error')); ?>

	            </div>
	        </div>
		</div>
		<?php endif; ?>

		<div class="row product-data">
			<div class="col-md-4 left-menu">
				<ul>
					<?php foreach($categories as $category): ?>
					<?php $link = str_replace(' ', '-', $category->title); ?>
					<li>
						<img src="/images/assets/<?php echo e($category->img_src); ?>" width="40" height="40">
						<a href="/product/category/<?php echo e($link); ?>"><?php echo e($category->title); ?></a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="col-md-8 product-info">
				<div class="col-md-5 product_image">
					<img src="/images/assets/fullsize/<?php echo e($product->product_id); ?>.jpg" class="img-responsive">
				</div>
				<div class="col-md-7">
					<?php echo Form::open(array('route' => 'add-to-cart')); ?>

					<?php echo Form::hidden('product_id', $product->product_id); ?>

					<?php echo Form::hidden('name', $product->variety); ?>

					<h1><?php echo e($product->variety); ?></h1>
					<p>	
						<style type="text/css">
							.fa {
								border: none !important;
							}
						</style>
						<p class="product_code">Rating: 
						<span>
						<?php if($rating == 1): ?>
							<i class="fa fa-star" aria-hidden="true"></i>
						<?php elseif($rating == 2): ?>
							<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
						<?php elseif($rating == 3): ?>
							<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
						<?php elseif($rating == 4): ?>
							<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
						<?php elseif($rating == 5): ?>
							<i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
						<?php endif; ?>
						</span></p>
						<p class="product_code">Product code: <span><strong><?php echo e($product->cataloguecode); ?></strong></span></p>
						<p class="product_ava">Availability: <span><strong><?php echo e($product->availability); ?></strong></span></p>
						<p class="product_unit">Unit of Purchase: <span><strong><?php echo e($product->tray_quantity); ?></strong></span></p>
						<div class="product_desc"><?php echo $product->prod_desc; ?></div>
						<?php if($data_sheet): ?>
							<p class="data_sheet"><a href="/images/assets/cat/spec-sheets/<?php echo e($product->product_id); ?>.pdf" class="datasheet"><img src="/images/assets/data-sheet.png"></a>
						<?php endif; ?>
						<?php if(JFH\Helpers::IsUserLoggedIn()): ?>
						<div class="break_table">
							<table id="miyazaki"">
							<caption>Price Break or <a href="/customer/rating/<?php echo e($product->product_id); ?>">Add a review</a></caption>
							<thead>
								<tr>
									<th>Qty Break</th>
									<th>Price Per Unit</th>	
									<?php if($product->priceper > $product->tray_quantity): ?>
										<th>Price Per <?php echo e($product->priceper); ?></th>
									<?php endif; ?>								
								</tr>
							</thead>
							<tbody>

							<?php if($product->break_a_desc): ?>
								<tr>
									<td><?php echo e($product->break_a_desc); ?></td>
									<td>&pound;<?php echo e(number_format($product->price_a, 2)); ?></td>
									<?php if($product->priceper > $product->tray_quantity): ?>
										<td>&pound;<?php echo e(number_format($product->priceper / $product->tray_quantity * $product->price_a, 2)); ?></td>
									<?php endif; ?>	
								</tr>
							<?php endif; ?>
							<?php if($product->break_b_desc): ?>
								<tr>
									<td><?php echo e($product->break_b_desc); ?></td>
									<td>&pound;<?php echo e(number_format($product->price_b, 2)); ?></td>
									<?php if($product->priceper > $product->tray_quantity): ?>
										<td>&pound;<?php echo e(number_format($product->priceper / $product->tray_quantity * $product->price_b, 2)); ?></td>
									<?php endif; ?>	
								</tr>
							<?php endif; ?>
							<?php if($product->break_c_desc): ?>
								<tr>
									<td><?php echo e($product->break_c_desc); ?></td>
									<td>&pound;<?php echo e(number_format($product->price_c, 2)); ?></td>
									<?php if($product->priceper > $product->tray_quantity): ?>
										<td>&pound;<?php echo e(number_format($product->priceper / $product->tray_quantity * $product->price_c, 2)); ?></td>
									<?php endif; ?>	
								</tr>
							<?php endif; ?>
							<?php if($product->break_d_desc): ?>
								<tr>
									<td><?php echo e($product->break_d_desc); ?></td>
									<td>&pound;<?php echo e(number_format($product->price_d, 2)); ?></td>
									<?php if($product->priceper > $product->tray_quantity): ?>
										<td>&pound;<?php echo e(number_format($product->priceper / $product->tray_quantity * $product->price_d, 2)); ?></td>
									<?php endif; ?>	
								</tr>
							<?php endif; ?>
							<?php if($product->break_e_desc): ?>
								<tr>
									<td><?php echo e($product->break_e_desc); ?></td>
									<td>&pound;<?php echo e(number_format($product->price_e, 2)); ?></td>
									<?php if($product->priceper > $product->tray_quantity): ?>
										<td>&pound;<?php echo e(number_format($product->priceper / $product->tray_quantity * $product->price_e, 2)); ?></td>
									<?php endif; ?>	
								</tr>
							<?php endif; ?>
							</tbody>
							</table>
						</div>
						<?php else: ?>
						<a href="/customer"><h4><strong>Please login to see prices</strong></h4></a>
						<?php endif; ?>
						<?php if(count($product_sizes) > 1): ?>
						<div class="col-md-12 product-select">
							<div class="row">
								<label>Select your variation</label>
								<?php echo e(Form::select('pot_size', $product_sizes, null, array('class' => 'size_select'))); ?>

							</div>
						</div>
						<?php endif; ?>
						<div class="col-md-12 product-select">
							<div class="row">
								<input type="text" name="qty" value="1">
								Choose Qty
							</div>
						</div>
						<div class="col-xs-12 col-md-6 order">
							<div class="row">
								<button>Add To Basket</button>
							</div>
						</div>
					<?php echo Form::close(); ?>

					<?php echo Form::open(array('route' => 'product-comparison')); ?>

					<?php echo Form::hidden('product_id', $product->product_id, array('id' => 'product_id')); ?>

						<div class="col-xs-12 col-md-6 comparison">
							<div class="row">
								<button>Add To Comparison</button>
							</div>
						</div>
					<?php echo Form::close(); ?>

				</div>
				<div class="col-md-12 col-xs-12 recently-viewed">
					<h2>Recently Viewed</h2>
					<?php foreach($recents as $recent): ?>
						<div class="col-md-4" style="margin: 20px 0">
							<div class="product-list-data">
								<h1 style="font-size: 18px;"><?php echo e($recent->variety); ?></h1>
								<div class="product-image">
									<img src="/images/assets/fullsize/<?php echo e($recent->product_id); ?>.jpg" class="img-responsive">
								</div>
								<div class="col-md-12">
									<div class="row">
										<?php $slug = str_replace(' ', '-', $recent->variety); ?>
										<?php $slug = str_replace('/', '-', $slug); ?>
										<a href="/product/<?php echo e($recent->product_id); ?>/<?php echo e($slug); ?>/"><button>More Info</button></a>
									</div>
								</div>
								<div style="clear: both"></div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(document).ready(function() {

			$('.datasheet').error(function() {
			  alert('Image does not exist !!');
			});

		    $( ".size_select" ).change(function() {
		    	loc = '/images/assets/fullsize/'+this.value+'.jpg'
				$('.col-md-5.product_image img').attr("src",loc);

				$.ajaxSetup({
				    headers: {
				        'X-CSRF-TOKEN': '<?php echo e(csrf_token()); ?>'
				    }
				});

				$.ajax({
				      type: "POST",
				      url: "/product/update",
				      data: {id: this.value},
				      success: function(data)
				      {	
				          console.log(data);
				      	  $('#product_id').val(data.product_id);
				          $('.product_desc').html(data.prod_desc);
				          $('.product_code span').html(data.cataloguecode);
				          $('.product_ava span').html(data.availability);
				          $('.product_unit span').html(data.tray_quantity);
				      }
				});

				$.ajax({
				      type: "POST",
				      url: "/product/update/break",
				      data: {id: this.value},
				      success: function(data)
				      {
				      		$('.break_table').html(data);
				      }
				});

			});
		});
	</script>


	<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>