<!DOCTYPE html>
<html>
<head>
	<?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
	<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Testimonials</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container testimonials">
		<div class="row">
			<?php foreach($testimonials as $testimonial): ?>
				<div class="col-md-4">
					<blockquote>
						<p><?php echo $testimonial->content; ?></p>
						<h3><?php echo e($testimonial->customer_name); ?></h3>
					</blockquote>
				</div>
			<?php endforeach; ?>
		</div>
	</div>

	</script>	

	<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>