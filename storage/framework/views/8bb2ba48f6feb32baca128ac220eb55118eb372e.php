<div class="container-fluid footer">
	<div class="container-fluid footer-overlay">
		<div class="container">
			<div class="row">
				<div class="col-md-3">
					<h3>About Us</h3>
					<p><?php echo JFH\Helpers::getSettings()->about_us; ?></p>
					<a href="/" title="JFH - Horticultural Supplies Ltd"><img src="/images/assets/footer-logo.png" alt="JFH - Horticultural Supplies Ltd" /></a>
					<div style="margin-top: 20px;"><img src="/images/assets/paypal.png" class="img-responsive" alt="PayPal" /></div>
				</div>
				<div class="col-md-3">
					<h3>Twitter Feed</h3>
					<a class="twitter-timeline" data-lang="en" data-height="250" data-dnt="true" data-link-color="#19CF86" href="https://twitter.com/JFHSandbach">Tweets by JFHSandbach</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
				</div>
				<div class="col-md-3">
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8&appId=741895615966075";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				<h3>Facebook Feed</h3>
				<div class="fb-page" data-href="https://www.facebook.com/JFHHorticultural/" data-tabs="timeline" data-height="300" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/JFHHorticultural/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/JFHHorticultural/">JFH Horticultural Supplies Ltd</a></blockquote></div>
				</div>
				<div class="col-md-3">
					<h3>Get in Touch</h3>
					<p class="address">JFH Horticultural Supplies Ltd,<br/>Unit 2, Lower Complex,<br/>Lodge Road,<br/>Sandbach,<br/>Cheshire<br/>CW11 3HP</p>
					<p class="telephone"><a href="tel:" onclick="ga('send', 'event', 'Mobile Click To Call', 'Call', 'Call Campaign');">01270 212 726</a></p>
					<p class="email"><a href="mailto:mail@jfhhorticultural.com">mail@jfhhorticultural.com</a></p>
					<p class="hours">Mon - Fri:  8:00 - 5:00</p>
					<img src="/images/assets/iso-9001.png" width="60%" class="img-responsive">
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 copyright">
					<span class="social">
						<a href="https://www.facebook.com/JFH-Horticultural-Supplies-Ltd-317365915270930/?hc_ref=SEARCH&fref=nf" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
						<a href="https://twitter.com/JFHSandbach" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a>
						<a href="https://plus.google.com/104803569020955114468" target="_blank"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
						<a href="https://www.linkedin.com/company/8239356?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A8239356%2Cidx%3A2-3-4%2CtarId%3A1472830244968%2Ctas%3AJFH" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
					</span>
					<p><a href="/terms">Terms &amp; Conditions</a> / <a href="/privacy">Privacy Policy</a></p>
					<p>&copy; <?php echo date("Y"); ?> JFH Horticultural Supplies Ltd</p>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
    $("#nav-mobile").html($("#nav-main").html());
    $("#nav-trigger span").click(function(){
        if ($("nav#nav-mobile ul").hasClass("expanded")) {
            $("nav#nav-mobile ul.expanded").removeClass("expanded").slideUp(250);
            $(this).removeClass("open");
        } else {
            $("nav#nav-mobile ul").addClass("expanded").slideDown(250);
            $(this).addClass("open");
        }
    });
});
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/57e1539ddd50b6145368dded/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Scripts-->
</body>
</html>