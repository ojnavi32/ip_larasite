<!DOCTYPE html>
<html>
<head>
	<?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
	<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Our Products</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container product search">
		<?php if(session('Good')): ?>
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-success">
	                <?php echo e(session('Good')); ?>

	            </div>
	        </div>
		</div>
		<?php endif; ?>
		<div class="row">
			<div class="col-md-5 searchbox">
				<?php echo Form::open(array('url' => '/search')); ?>

					<input type="text" name="search" placeholder="Search"><button><i class="fa fa-search" aria-hidden="true"></i></button>
				<?php echo Form::close(); ?>

			</div>
		</div>

		<div class="row product-data">
			<div class="col-md-4 left-menu">
				<ul>
					<?php foreach($categories as $category): ?>
					<?php $link = str_replace(' ', '-', $category->title); ?>
					<li>
						<img src="/images/assets/<?php echo e($category->img_src); ?>" width="40" height="40">
						<a href="/product/category/<?php echo e($link); ?>"><?php echo e($category->title); ?></a>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>
			<div class="col-md-8 product-list">
				<div class="row">
					<?php foreach($products as $product): ?>
						<div class="col-md-4">
							<div class="product-list-data">
								<?php $slug = str_replace(' ', '-', $product->variety); ?>
								<?php $slug = str_replace('/', '-', $slug); ?>
								<a href="/product/<?php echo e($product->product_id); ?>/<?php echo e($slug); ?>/">
								<h1><?php echo e($product->variety); ?></h1>
								<div class="product-image">
									<img src="/images/assets/fullsize/<?php echo e($product->product_id); ?>.jpg" class="img-responsive">
								</div>
								<div class="col-md-12">
									<div class="row">
										<button>More Info</button>
									</div>
								</div>
								<div style="clear: both"></div>
								</a>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
		    $('.product-list-data').matchHeight();
		});
	</script>	

	<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>