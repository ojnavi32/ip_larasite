<!DOCTYPE html>
<html>
<head>
	<?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
	<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Our Products</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container categories">
		<?php if(session('Good')): ?>
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-success">
	                <?php echo e(session('Good')); ?>

	            </div>
	        </div>
		</div>
		<?php endif; ?>
		<div class="row">
			<div class="col-md-12">
				<div class="row categories">
					<?php foreach($categories as $category): ?>
					<?php $link = str_replace(' ', '-', $category->title); ?>
					<a href="/product/category/<?php echo e($link); ?>">
					<div class="col-md-3 center-text">
						<img src="/images/assets/<?php echo e($category->img_src); ?>">						
						<h4><?php echo e($category->title); ?></h4>
					</div></a>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
		    $('.product-list-data').matchHeight();
		});
	</script>	

	<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>