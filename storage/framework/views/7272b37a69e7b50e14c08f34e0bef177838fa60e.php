<!DOCTYPE html>
<html>
<head>
	<?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
	<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Your Cart</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container cart">
		<div class="col-md-12">
	
			<?php if(session('Removed')): ?>
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="row">
				    <div class="alert alert-success">
		                <?php echo e(session('Removed')); ?>

		            </div>
		        </div>
			</div>
			<?php endif; ?>
			<?php if(session('Good')): ?>
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="row">
				    <div class="alert alert-success">
		                <?php echo e(session('Good')); ?>

		            </div>
		        </div>
			</div>
			<?php endif; ?>
			<?php if(session('Updated')): ?>
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="row">
				    <div class="alert alert-success">
		                <?php echo e(session('Updated')); ?>

		            </div>
		        </div>
			</div>
			<?php endif; ?>
			<?php if(session('Error')): ?>
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="row">
				    <div class="alert alert-warning">
		                <?php echo e(session('Error')); ?>

		            </div>
		        </div>
			</div>
			<?php endif; ?>

			<table id="cart" class="table table-hover table-condensed">
				<thead>
					<tr>
						<th style="width:50%">Product</th>
						<th style="width:10%;text-align: center;">Price</th>
						<th style="width:8%;text-align: center;">Quantity</th>
						<th style="width:8%;text-align: center;">Total</th>
						<th style="width:10%"></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach(Cart::content() as $row): ?>
					<tr>
						<td data-th="Product">
							<div class="row">
								<div class="col-sm-2 hidden-xs"><img src="/images/assets/thmbs/<?php echo e($row->id); ?>.jpg" class="img-responsive"/></div>
								<div class="col-sm-10">
									<h4 class="nomargin"><?php echo e($row->name); ?></h4>
									<span class="cart-description"><?php echo $row->options->size; ?></span>
									<span class="cart-description"><?php echo $row->options->description; ?></span>
								</div>
							</div>
						</td>
						<td data-th="Price">&pound;<?php echo e(number_format($row->price,2)); ?></td>
							<?php echo Form::open(array('route' => 'update-item', 'class' => 'cart-form')); ?>

							<?php echo Form::hidden('item_id', $row->id); ?>

							<?php echo Form::hidden('cart_row_id', $row->rowId); ?>

							<td data-th="Quantity">
								<input type="number" name="qty" class="form-control text-center" value="<?php echo e($row->qty); ?>">
							</td>
							<td data-th="Price">&pound;<?php echo e(number_format($row->price * $row->qty,2)); ?></td>
							<td class="actions" data-th="">
								<button class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button>
							<?php echo Form::close(); ?>	
								<?php echo Form::open(array('route' => 'remove-item', 'class' => 'cart-form')); ?>

							<?php echo Form::hidden('cart_row_id', $row->rowId); ?>

								<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
							<?php echo Form::close(); ?>						
						</td>

					</tr>					
			        <?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr class="visible-xs">
						<td class="text-center"><strong>Total <?php echo e(Cart::subtotal()); ?></strong></td>
					</tr>
					<?php if(Cart::subtotal() > 200): ?>
					<tr>
						<td colspan="3"></td>
						<td class="hidden-xs text-center"><strong>Shipping &pound;0.00</strong></td>
						<td colspan="1"></td>
					</tr>
					<?php else: ?>
					<tr>
						<td colspan="3"></td>
						<td class="hidden-xs text-center"><strong>Shipping &pound;9.00</strong></td>
						<td colspan="1"></td>
					</tr>
					<?php endif; ?>
					<tr>
						<td colspan="3"></td>
						<td class="hidden-xs text-center"><strong>Sub Total <?php if(Cart::subtotal() > 200): ?> &pound;<?php echo e(Cart::subtotal()); ?> <?php else: ?> &pound;<?php echo e(number_format(Cart::subtotal()+9.00,2)); ?> <?php endif; ?></strong></td>
						<td colspan="1"></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td class="hidden-xs text-center">
						<strong>Vat 
							<?php if(Cart::subtotal() > 200): ?> 
								&pound;<?php echo e(Cart::tax()); ?> 
							<?php else: ?>
								<?php
								$total = Cart::subtotal()+9.00;
								$vat = $total * 0.20;
								$totalIncVat = $vat;
								?>
								&pound;<?php echo e(number_format($totalIncVat,2)); ?> 
							<?php endif; ?>
						</strong></td>
						<td colspan="1"></td>
					</tr>
					<tr>
						<td><a href="/product/categories" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
						<td colspan="2" class="hidden-xs"></td>
						<td class="hidden-xs text-center"><strong>Total &pound;<?php if(Cart::subtotal() > 200): ?> <?php echo e(Cart::total()); ?> <?php else: ?> <?php echo e(number_format($total + $totalIncVat,2)); ?> <?php endif; ?></strong></td>
						<td><a href="/cart/check-payment" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>


	<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>