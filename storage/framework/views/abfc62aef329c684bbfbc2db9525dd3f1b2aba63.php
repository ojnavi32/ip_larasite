<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        <?php if(session('Updated')): ?>
            <div class="alert alert-success">
                <?php echo e(session('Updated')); ?>

            </div>
        <?php endif; ?>

            <div class="panel panel-default">
                <div class="panel-heading">Edit Product</div>
                <?php echo Form::open(array('route' => array('admin.products.update', $product->product_id), 'method' => 'put')); ?>

                <div class="panel-body">
                    <h1></h1>
                    <div class="col-md-12">
                        <label for="inputTitle">Variety</label>
                        <input type="text" name="variety" value="<?php echo e($product->variety); ?>" id="variety" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Pot Size</label>
                        <input type="text" name="pot_size" value="<?php echo e($product->pot_size); ?>" id="pot_size" class="form-control" autofocus="">

                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Product Range</label>
                        <input type="text" name="product_range" value="<?php echo e($product->product_range); ?>" id="product_range" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">State</label>
                        <input type="text" name="state" value="<?php echo e($product->state); ?>" id="state" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Stock</label>
                        <input type="text" name="stock" value="<?php echo e($product->stock); ?>" id="stock" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Tray Quantity</label>
                        <input type="text" name="tray_quantity" value="<?php echo e($product->tray_quantity); ?>" id="tray_quantity" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Category</label>
                        <input type="text" name="catagory" value="<?php echo e($product->catagory); ?>" id="catagory" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Availability</label>
                        <input type="text" name="availability" value="<?php echo e($product->availability); ?>" id="availability" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Recomended</label>
                        <input type="text" name="recomended" value="<?php echo e($product->recomended); ?>" id="recomended" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Cataloguecode</label>
                        <input type="text" name="cataloguecode" value="<?php echo e($product->cataloguecode); ?>" id="cataloguecode" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Form</label>
                        <input type="text" name="form" value="<?php echo e($product->form); ?>" id="form" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Price A</label>
                        <input type="text" name="price_a" value="<?php echo e($product->price_a); ?>" id="price_a" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Price B</label>
                        <input type="text" name="price_b" value="<?php echo e($product->price_b); ?>" id="price_b" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Price C</label>
                        <input type="text" name="price_c" value="<?php echo e($product->price_c); ?>" id="price_c" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Price D</label>
                        <input type="text" name="price_d" value="<?php echo e($product->price_d); ?>" id="price_d" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Price E</label>
                        <input type="text" name="price_e" value="<?php echo e($product->price_e); ?>" id="price_e" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Break Point A</label>
                        <input type="text" name="break_a" value="<?php echo e($product->break_a); ?>" id="break_a" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Break Point B</label>
                        <input type="text" name="break_b" value="<?php echo e($product->break_b); ?>" id="break_b" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Break Point C</label>
                        <input type="text" name="break_c" value="<?php echo e($product->break_c); ?>" id="break_c" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Price Break D</label>
                        <input type="text" name="break_d" value="<?php echo e($product->break_d); ?>" id="break_d" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Price Break E</label>
                        <input type="text" name="break_e" value="<?php echo e($product->break_e); ?>" id="break_e" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Break A Desc</label>
                        <input type="text" name="break_a_desc" value="<?php echo e($product->break_a_desc); ?>" id="break_a_desc" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Break B Desc</label>
                        <input type="text" name="break_b_desc" value="<?php echo e($product->break_b_desc); ?>" id="break_b_desc" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Break C Desc</label>
                        <input type="text" name="break_c_desc" value="<?php echo e($product->break_c_desc); ?>" id="break_c_desc" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Break D Desc</label>
                        <input type="text" name="break_d_desc" value="<?php echo e($product->break_d_desc); ?>" id="break_d_desc" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Break E Desc</label>
                        <input type="text" name="break_e_desc" value="<?php echo e($product->break_e_desc); ?>" id="break_e_desc" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Vatcode</label>
                        <input type="text" name="vatcode" value="<?php echo e($product->vatcode); ?>" id="vatcode" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">UOP</label>
                        <input type="text" name="uop" value="<?php echo e($product->uop); ?>" id="uop" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Barcode</label>
                        <input type="text" name="barcode" value="<?php echo e($product->barcode); ?>" id="barcode" class="form-control" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputTitle">Deparment ID</label>
                        <input type="text" name="department_id" value="<?php echo e($product->department_id); ?>" id="department_id" class="form-control" autofocus="">
                    </div>

                    <div class="col-md-12">
                        <label for="inputTitle">Price Per</label>
                        <input type="text" name="priceper" value="<?php echo e($product->priceper); ?>" id="priceper" class="form-control" autofocus="">
                    </div>
                    
                    <div class="col-md-12">
                        <label>Product Description</label>
                        <textarea id="summernote" name="prod_desc"><?php echo e($product->prod_desc); ?></textarea>
                    </div>

                    <input type="submit" value="Update">
                <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 300
      });
    });

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>