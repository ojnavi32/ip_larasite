<!DOCTYPE html>
<html>
<head>
	<?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
	<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

	<div class="container-fluid contact-header">
		<div class="container-fluid contact-overlay">
			<div class="container contact-header-text">
				<div class="row">
					<span class="breadcrumbs-active">home > </span><span class="breadcrumbs"> login / register</span>
				</div>
				<div class="row">
					<div class="contact col-md-3 col-centered bottom-shadow">
						<h1>Your Account</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container register-login">
		<div class="col-md-12">
	
			<?php if(session('Good')): ?>
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="row">
				    <div class="alert alert-success">
		                <?php echo e(session('Good')); ?>

		            </div>
		        </div>
			</div>
			<?php endif; ?>

			<?php if(session('Error')): ?>
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="row">
				    <div class="alert alert-warning">
		                <?php echo e(session('Error')); ?>

		            </div>
		        </div>
			</div>
			<?php endif; ?>

            <div class="row">
                <div class="col-sm-5">
                	
                	<div class="form-box">
                    	<div class="form-top">
                    		<div class="form-top-left">
                    			<h3>Login to our site</h3>
                        		<p>Enter email address and password to log in:</p>
                    		</div>
                        </div>
                        <div class="form-bottom">
		                    <?php echo Form::open(array('route' => 'customer-login')); ?>

		                    	<div class="form-group">
		                    		<label class="sr-only" for="form-username">Username</label>
		                        	<input type="text" name="username" placeholder="Email Address" class="form-username form-control" id="form-username">
		                        </div>
		                        <div class="form-group">
		                        	<label class="sr-only" for="form-password">Password</label>
		                        	<input type="password" name="password" placeholder="Password" class="form-password form-control" id="form-password">
		                        </div>
		                        <button type="submit" class="btn">Sign in</button>
		                    <?php echo Form::close(); ?>

	                    </div>
                    </div>
                </div>
                
                <div class="col-sm-1 middle-border"></div>
                <div class="col-sm-1"></div>
                	
                <div class="col-sm-5">
                	
                	<div class="form-box">
                		<div class="form-top">
                    		<div class="form-top-left">
                    			<h3>Sign up now</h3>
                        		<p>Fill in the form below to apply for an account:</p>
                    		</div>
                        </div>
                        <div class="form-bottom">
		                    <?php echo Form::open(array('route' => 'customer-register')); ?>

		                    	<div class="form-group">
		                    		<label class="sr-only" for="form-first-name">First name</label>
		                        	<input type="text" name="fname" placeholder="First name" class="form-first-name form-control" id="form-first-name" required>
		                        </div>
		                    	<div class="form-group">
		                    		<label class="sr-only" for="form-first-name">Last name</label>
		                        	<input type="text" name="lname" placeholder="Last name" class="form-first-name form-control" id="form-first-name" required>
		                        </div>
		                        <div class="form-group">
		                        	<label class="sr-only" for="form-email">Email</label>
		                        	<input type="text" name="email" placeholder="Email" class="form-email form-control" id="form-email" required>
		                        </div>
		                        <div class="form-group">
		                        	<label class="sr-only" for="form-email">Password</label>
		                        	<input type="password" name="password" placeholder="Password" class="form-email form-control" id="form-email" required>
		                        </div>
		                        <div class="form-group">
		                        	<label class="sr-only" for="form-email">Company Name</label>
		                        	<input type="text" name="company_name" placeholder="Company Name" class="form-email form-control" id="form-email" required>
		                        </div>
		                        <div class="form-group">
		                        	<label class="sr-only" for="form-email">Mobile Number</label>
		                        	<input type="text" name="mobile_number" placeholder="Mobile Number" class="form-email form-control" id="form-email" required>
		                        </div>
		                        <div class="form-group">
		                        	<label class="sr-only" for="form-email">Phone Number</label>
		                        	<input type="text" name="phone_number" placeholder="Phone Number" class="form-email form-control" id="form-email" required>
		                        </div>
		                        <div class="form-group">
		                        	<div class="col-md-12"></div>
		                        </div>
		                        <div class="form-group">
		                        	<label class="sr-only" for="form-email">Address Line 1</label>
		                        	<input type="text" name="address_1" placeholder="Address Line 1" class="form-email form-control" id="form-email" required>
		                        </div>
		                        <div class="form-group">
		                        	<label class="sr-only" for="form-email">Address Line 2</label>
		                        	<input type="text" name="address_2" placeholder="Address Line 2" class="form-email form-control" id="form-email">
		                        </div>
		                        <div class="form-group">
		                        	<label class="sr-only" for="form-email">Address Line 3</label>
		                        	<input type="text" name="address_3" placeholder="Address Line 3" class="form-email form-control" id="form-email">
		                        </div>
		                        <div class="form-group">
		                        	<label class="sr-only" for="form-email">Post Code</label>
		                        	<input type="text" name="post_code" placeholder="Post Code" class="form-email form-control" id="form-email" required>
		                        </div>
		                        <button type="submit" class="btn">Sign me up</button>
		                    </form>
	                    </div>
                	</div>
                	
                </div>
            </div>

		</div>
	</div>


	<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>