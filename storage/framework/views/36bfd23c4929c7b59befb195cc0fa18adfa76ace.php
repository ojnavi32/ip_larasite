<!DOCTYPE html>
<html>
<head>
	<?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
	<?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
		<div class="container-fluid white" style="padding-top: 0px !important;">
			<div class="container  error-404">
				<div class="col-md-6">
					<h1>404</h1>
				</div>
				<div class="col-md-6 links-404">
					<p>Well, this is embarrassing, you seem to have found a broken or dead link. Below are some of our most used pages, maybe try one of these:</p>
					<ul>
						<li><a href="/">HOME</a></li>
						<li><a href="http://publizr.com/jfhhorticultural/jfh-catalogue-2016">CATALOGUE</a></li>
						<li><a href="/product/categories">ONLINE ORDERING</a></li>
						<li><a href="/testimonials">TESTIMONIALS</a></li>
						<li><a href="/blog">BLOG</a></li>
						<li><a href="/contact-us">CONTACT US</a></li>
					<ul>
				</div>
			</div>
		</div>
		<?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>
