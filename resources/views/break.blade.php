							<table id="miyazaki"">
							<caption>Price Break</caption>
							<thead>
								<tr>
									<th>Qty Break</th>
									<th>Price Per Unit</th>	
									@if($product->priceper >  1 || $product->priceper > $product->tray_quantity)
										<th>Price Per {{$product->priceper}}</th>
									@endif								
								</tr>
							</thead>
							<tbody>

							@if($product->break_a_desc)
								<tr>
									<td>{{$product->break_a_desc}}</td>
									<td>&pound;{{number_format($product->price_a, 2)}}</td>
									@if($product->priceper >  1 || $product->priceper > $product->tray_quantity)
										<td>&pound;{{number_format($product->priceper / $product->tray_quantity * $product->price_a, 2)}}</td>
									@endif	
								</tr>
							@endif
							@if($product->break_b_desc)
								<tr>
									<td>{{$product->break_b_desc}}</td>
									<td>&pound;{{number_format($product->price_b, 2)}}</td>
									@if($product->priceper >  1 || $product->priceper > $product->tray_quantity)
										<td>&pound;{{number_format($product->priceper / $product->tray_quantity * $product->price_b, 2)}}</td>
									@endif	
								</tr>
							@endif
							@if($product->break_c_desc)
								<tr>
									<td>{{$product->break_c_desc}}</td>
									<td>&pound;{{number_format($product->price_c, 2)}}</td>
									@if($product->priceper >  1 || $product->priceper > $product->tray_quantity)
										<td>&pound;{{number_format($product->priceper / $product->tray_quantity * $product->price_c, 2)}}</td>
									@endif	
								</tr>
							@endif
							@if($product->break_d_desc)
								<tr>
									<td>{{$product->break_d_desc}}</td>
									<td>&pound;{{number_format($product->price_d, 2)}}</td>
									@if($product->priceper >  1 || $product->priceper > $product->tray_quantity)
										<td>&pound;{{number_format($product->priceper / $product->tray_quantity * $product->price_d, 2)}}</td>
									@endif	
								</tr>
							@endif
							@if($product->break_e_desc)
								<tr>
									<td>{{$product->break_e_desc}}</td>
									<td>&pound;{{number_format($product->price_e, 2)}}</td>
									@if($product->priceper >  1 || $product->priceper > $product->tray_quantity)
										<td>&pound;{{number_format($product->priceper / $product->tray_quantity * $product->price_e, 2)}}</td>
									@endif	
								</tr>
							@endif
							</tbody>
							</table>