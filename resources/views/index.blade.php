<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
</head>
<body>
	@include('includes.header-index')
	

		<div class="container-fluid white">
			<div class="container">
				<div class="row">
					<div class="col-md-12 center-text">
						<h3>Our <span class="green">Products</span></h3><br/>
						<img class="icons-margin" src="/images/assets/tools.png">
					</div>
				</div>
				
				<div class="row categories">
					@foreach($categories as $category)
					<?php $link = str_replace(' ', '-', $category->title); ?>
					<a href="/product/category/{{$link}}">
					<div class="col-md-3 center-text">
						<img src="/images/assets/{{$category->img_src}}">						
						<h4>{{$category->title}}</h4>
					</div></a>
					@endforeach
				</div>

			</div>
		</div>

		<div class="container-fluid ordering-bg">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-6 order-box">
						<h2>Save Time and Order Online</h2>
						<p>It’s easier and faster ordering online. Register your details with us and have a stress free ordering experience.</p>
						<h2>Do you have a log in?</h2>
						<p>If not, all you need to do is ‘Register’ above and complete your details.  You will have an online account within 24 hours.</p>
						<a href="/customer"><button>Read More <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button></a>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid white">
			<div class="container">
				<div class="row">
					<div class="col-md-12 center-text">
						<h3>Why Use <span class="green">JFH Horticultural Supplies</span></h3><br/>
						<p>Our company is committed to understanding and attending to the customers' needs in the most efficient and cost effective manner.</p>
						<img class="icons-margin" src="/images/assets/why-icon.png">
					</div>
				</div>

				<div class="row why-reasons">
					<div class="col-md-4">
						<h2>Alertness</h2>
						<p>Our Company is committed to take the lead in providing goods, services and solutions with a rapid response to help our clients grow.</p>
					</div>
					<div class="col-md-4">
						<h2>Continuity</h2>
						<p>Here to supply goods and service, today, tomorrow and whenever the customer requires them.</p>
					</div>
					<div class="col-md-4">
						<h2>Synergy</h2>
						<p>We work with you to achieve what neither of us can do on our own.</p>
					</div>
				</div>
				<div class="row why-reasons">
					<div class="col-md-4">
						<h2>Frankness</h2>
						<p>No false promises, no hidden costs. What we say is what you get and if we can't we will tell you.</p>
					</div>
					<div class="col-md-4">
						<h2>ISO 9001 accredited</h2>
						<p>We are ISO 9001 <strong>accredited</strong> so you can rest assured that we are professional &amp; trustworthy</p>
					</div>
					<div class="col-md-4">
						<h2>Unique Product Sourcing</h2>
						<p>Continually providing solutions for our customers’ needs and problems.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="container-fluid experience-team">
			<div class="container">
				<div class="row">
					<div class="col-md-12 center-text">
							<h3>Experienced <span class="green">Team</span></h3><br/>
							<img class="icons-margin" src="/images/assets/experienced.png" class="img-responsive">
							<h4>Helping you Grow today…Helping you Bloom tomorrow…</h4>
					</div>
				</div>
	
				<div class="row">
					<div class="col-md-4 green-social-outer ">
						<div class="green-social-inner">
							<p>JFH are a family owned and operated company and have an amazing team of dedicated employees throughout our organisation. We have been on a journey which started out with Doug Hutchins who started to sell sundries after diversifying from the original landscaping and growing business which was founded by Joe Hutchins over 60 years ago.<p/>Now boasting a truly unique family business with nine family members working within the organisation. <p>We think of our customers as our extended family, and want them to have the best. We feel it our duty to ensure through prudent long term business management practices and an eye on the future we have what it takes to be your trusted partner. Family takes care of its own – join ours, you’ll be glad you did.</p>
						</div>
					</div>
					<div class="col-md-8 exp_image team_image">
					</div>
				</div>
				<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-browser/0.1.0/jquery.browser.js"></script>
				<script type="text/javascript">
					var win   = $.browser.win;
					var linux = $.browser.linux;
					if (win == true || linux == true) {
						$(".col-md-8.exp_image.team_image").addClass("windows-image");
					}
				</script>
			</div>
		</div>

		<div class="container-fluid white" style="padding-top: 0px !important;">
			<div class="container">
				<!--<div class="row">
					<div class="col-md-12 center-text">
						<h3>Testi<span class="green">monials</span></h3><br/>
						<p>magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia.</p>
					</div>
				</div>
				
				<div class="row">
					<div class="col-md-12 center-text">
						<div class="testimonials">
							<p>2 days ago by Gold medal winner at Chelsea Flower Show, Nursery, Cheshire</p>
							<p>We have dealt with JFH Horticultural for many years, they have consistently offered the highest level of service and quality. We recommend JFH to anyone anytime.</p>
						</div>
					</div>
				</div>-->

				<!--<div class="row news">
					<div class="col-md-12 center-text">
						<img class="icons-margin" src="/images/assets/plant.png">
						<h3>Latest News &amp; <span class="green">Tips</span></h3><br/>
						<p>magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia.</p>
					</div>
				</div>

				<div class="row news-items">
					@foreach($posts as $post)
						<div class="col-md-4">
							<div class="news-item-header" style="background: url(/images/blog/{{$post->filename}}); background-size: cover;">
							</div>
							<div class="news-item-body">
								<h5><a href="/blog">{{$post->title}}</a></h5>
								<span class="news-item-date">JFH |  {{$post->created_at->format('jS \of F Y')}}</span>
								<p class="news-item-text">{!! str_limit($post->content) !!}</p>
							</div>
						</div>
					@endforeach
				</div>-->
			</div>
		</div>
		@include('includes.footer')
</body>
</html>
