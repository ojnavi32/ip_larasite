@extends('layouts.customer.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h1 style="font-size: 18px; margin-bottom: 20px;">Credit Application Form</h1>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

        </div>
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Customer Information</div>
                <div class="panel-body">
                    {!! Form::open(array('url' => 'customer/credit-application/submit')) !!}
                      <div class="form-group">
                        <label for="NameOfBusiness">Name Of Business *</label>
                        <input type="text" class="form-control" name="NameOfBusiness" id="NameOfBusiness" placeholder="Name Of Business">
                      </div>
                      <div class="form-group">
                        <label for="Address">Address *</label>
                        <input type="text" name="address" class="form-control" id="Address" placeholder="Address">
                      </div>
                      <div class="form-group">
                        <label for="Address1">Address 1 *</label>
                        <input type="text" name="address1" class="form-control" id="Address1" placeholder="Address1">
                      </div>
                      <div class="form-group">
                        <label for="Town">Town *</label>
                        <input type="text" name="town" class="form-control" id="Town" placeholder="Town">
                      </div>
                      <div class="form-group">
                        <label for="PostCode">Post Code *</label>
                        <input type="text" name="postcode" class="form-control" id="PostCode" placeholder="Post Code">
                      </div>
                      <div class="form-group">
                        <label class="radio-inline">
                          <input type="radio" name="business_type" id="inlineRadio1" value="SoleTrader"> Sole Trader
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="business_type" id="inlineRadio2" value="Part"> Partnership
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="business_type" id="inlineRadio3" value="option3"> Limited Company
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="business_type" id="inlineRadio3" value="option3"> Other
                        </label>
                      </div>
                      <div class="form-group">
                        <label for="CompanyRegNo">Co Reg No: *</label>
                        <input type="text" name="companyregno" class="form-control" id="CompanyRegNo" placeholder="Compnay Registration Number">
                      </div>
                      <div class="form-group">
                        <label for="compnay_extra_info">List names and addresses of the sole owner/partners or main directors of business, List any parent, associate or subsiduary companies:</label>
                        <textarea name="extra_company_info" class="form-control" rows="10"></textarea>
                      </div>
                      <div class="form-group">
                        <label>Payment Method *</label></br>
                        <label class="radio-inline">
                          <input type="radio" name="payment_method" id="inlineRadio1" value="SoleTrader"> Cheque
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="payment_method" id="inlineRadio2" value="Part"> Direct Debit
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="payment_method" id="inlineRadio3" value="option3"> BACS
                        </label>
                        <label class="radio-inline">
                          <input type="radio" name="payment_method" id="inlineRadio3" value="option3"> Other
                        </label>
                      </div>
                     <div class="form-group">
                        <label for="credit_limit">Proposed Credit Limit (exc Vat): *</label>
                        <input type="text" name="credit_limit" class="form-control" id="credit_limit" placeholder="£500">
                      </div>
                      <div class="form-group">
                        <label for="compnay_extra_info">1st Trade Reference: *</label>
                        <textarea name="trade_references" class="form-control" rows="10"></textarea>

                        <label for="compnay_extra_info">2nd Trade Reference: *</label>
                        <textarea name="2ndtrade_references" class="form-control" rows="10"></textarea>
                      </div>
                </div>
            </div>
            <div class="panel panel-default">
            <div class="panel-heading">Bank Information *</div>
                <div class="panel-body">
                  <div class="form-group">
                    <label for="BankName">Bank Name:</label>
                    <input type="text" name="bank_name" class="form-control" id="BankName" placeholder="Bank Name">
                  </div>
                  <div class="form-group">
                    <label for="BankAddress">Bank Address:</label>
                    <input type="text" name="bank_address" class="form-control" id="BankAddress" placeholder="Bank Address">
                  </div>
                  <div class="form-group">
                    <label for="SortCode">Sort Code:</label>
                    <input type="text" name="sort_code" class="form-control" id="SortCode" placeholder="Sort Code">
                  </div>
                  <div class="form-group">
                    <label for="AccountNumber">Account Number:</label>
                    <input type="text" name="account_number" class="form-control" id="AccountNumber" placeholder="Account Number">
                  </div>
                </div>
            </div>
            <div class="panel panel-default">
            <div class="panel-heading">Additional Information *</div>
                <div class="panel-body">
                <p>I/We authorise you to take up references at any time from the above mentioned bank and trade suppliers including credit reference searches against both the business and the partners/directors personally and understand you will keep a record of those searches and mayl share that information with other businesses. I/We also agree to comply with your settlement terms (specified within your conditions of sale) and acknowledge safe receipt of a copy of these terms and conditions. A sample of our letter headed paper is attached hereto.</p>

              <div class="form-group">
                <label for="compnay_extra_info">References: *</label>
                <textarea name="references" class="form-control" rows="100"></textarea>
              </div>
              <div class="form-group">
                <label for="compnay_extra_info">Have you read our <a href="/pdf/credit-terms.pdf" target="_blank">Terms &amp; Conditions</a>: *</label>
                <input type="checkbox" name="terms" value="1">
              </div>
            <button type="submit" class="btn btn-default">Submit</button>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection
