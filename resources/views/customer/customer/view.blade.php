@extends('layouts.customer.app')

@section('content')
<div class="container">
    @if(session('Ok'))
        <div class="col-md-12" style="margin-top: 30px;">
            <div class="row">
                <div class="alert alert-success">
                    {{ session('Ok') }}
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title">
                <h2>Invoice</h2><h3 class="pull-right">Order # {{$order_id}}</h3>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-6">
                    <address>
                    <strong>Billed To:</strong><br>
                        {{$customer->company}}<br>
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                    <strong>Shipped To:</strong><br>
                        {{$customer->company}}<br>
                    </address>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <address>
                        <strong>Payment Method:</strong><br>
                        {{$order->payment_type}}<br>
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong>Order Date:</strong><br>
                        {{date('F d, Y', strtotime($order->dateoforder)) }}<br><br>
                    </address>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Order summary</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <td><strong>Item</strong></td>
                                    <td class="text-center"><strong>Quantity</strong></td>
                                    <td class="text-center"><strong>Tray </strong></td>
                                    <td class="text-center"><strong>Price</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                @foreach($orderitems as $orderitem)
                                    <tr>
                                        <td>{{ucwords($orderitem->variety)}} - {{$orderitem->pot_size}}</td>
                                        <td class="text-center">{{$orderitem->qty}}</td>
                                        <td class="text-center">{{$orderitem->tray_quantity}}</td>
                                        <td class="text-center">&pound;{{$orderitem->price}}</td>                                        
                                    </tr>
                                @endforeach
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Shipping</strong></td>
                                    <td class="no-line text-right">{{ $order->payment_gross > 200 ? "£0.00" : "£9.00" }}</td>
                                </tr>
                                <tr>
                                    <td class="no-line"></td>
                                    <td class="no-line"></td>
                                    <td class="no-line text-center"><strong>Total</strong></td>
                                    <td class="no-line text-right">&pound;{{$order->payment_gross}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
