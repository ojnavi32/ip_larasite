@extends('layouts.customer.app')

@section('content')
<div class="container">
<div class="row">
    
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Track your parcel</div>
            <div class="panel-body">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Was your parcel sent on a pallet?</div>
                        <div class="panel-body">
                            If your parcel was sent on a pallet use the tracking system below.
                            {!! Form::open(array('route' => 'track-order-pallet', 'style' => 'margin-top: 10px;')) !!}
                              <div class="form-group">
                                <label for="connumber">Consignment Number</label>
                                <input type="text" class="form-control" name="connumber" id="connumber" placeholder="ST85049396">
                              </div>
                              <div class="form-group">
                                <label for="postcode">Delivery Postcode</label>
                                <input type="text" class="form-control" name="postcode" id="postcode" placeholder="WA11 2UN">
                              </div>
                              <button>Go</button>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">Was your parcel dispatched in a box, carton or envelope?</div>
                        <div class="panel-body">
                            If your parcel was shipped in a box, carton or envelope use the system below.
                            {!! Form::open(array('route' => 'track-order-pallet', 'style' => 'margin-top: 10px;')) !!}
                              <div class="form-group">
                                <label for="connumber">Consignment Number</label>
                                <input type="text" class="form-control" name="connumber" id="connumber" placeholder="ST85049396">
                              </div>
                              <button>Go</button>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>                    
            </div>
        </div>
    </div>
</div>
</div>
@endsection
