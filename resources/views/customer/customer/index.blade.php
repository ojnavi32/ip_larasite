@extends('layouts.customer.app')

@section('content')
<div class="container">
<div class="row">
    
    <div class="col-sm-12">
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>#</th> 
                    <th>Order Ref</th>
                    <th>Status</th>
                    <th>Total</th>
                    <th>Total Vat Charged</th>
                    <th>Ordered At</th>
                    <th>Actions</th>
                </tr> 
            </thead> 
            <tbody> 
                @foreach($orders as $order)
                <tr> 
                    <td></td>
                    <td>{{$order->orders_id}}</td>
                    <td>{{$order->OrderState->order_status}}</td> 
                    <td>&pound;{{$order->payment_gross}}</td>
                    <td>&pound;{{$order->vat_charged}}</td>
                    <td>{{ date('d-m-Y H:i:s', strtotime($order->timedate)) }} </td>
                    <td><a href="{{ url('/customer/orders/view', [$order->orders_id]) }}">View Order</a></td>
                </tr> 
                @endforeach
            </tbody> 
        </table>
    </div>
</div>
</div>
@endsection
