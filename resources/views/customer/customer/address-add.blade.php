@extends('layouts.customer.app')

@section('content')
<script src="https://getaddress.io/js/jquery.getAddress-2.0.5.min.js"></script>

<div class="container">
<div class="row">
    
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Add Address</div>
            <div class="panel-body">
              {!! Form::open(array('url' => '/customer/address/store', 'onsubmit' => 'return postForm()')) !!}
              <!-- Postcode field -->
              <div id="postcode_lookup"></div>  
              <div style="margin-top:15px"></div>
              <div class="form-group">
                <label>First Address Line</label>
                <input id="line1" name="line1" class="form-control" type="text">
              </div>

              <div class="form-group">
                <label>Second Address Line</label>
                <input id="line2" name="line2" class="form-control" type="text">
              </div> 

              <div class="form-group">
                <label>Third Address Line</label>
                <input id="line3" name="line3" class="form-control" type="text">
              </div>
              
              <div class="form-group">
                <label>Town</label>
                <input id="town" name="town" class="form-control" type="text">
              </div>

              <div class="form-group">
                <label>County</label>
                <input id="county" name="county" class="form-control" type="text">
              </div>

              <div class="form-group">
                <label>Postcode</label>
                <input id="postcode" name="postcode" class="form-control" type="text">
              </div>
              <button id="getaddress">Go</button>
              {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
$('#postcode_lookup').getAddress({
    api_key: 'C9vsiZALN0m-_3HfEr_ikg6663',  
    output_fields:{
        line_1: '#line1',
        line_2: '#line2',
        line_3: '#line3',
        post_town: '#town',
        county: '#county',
        postcode: '#postcode'
    },
});

</script>
@endsection