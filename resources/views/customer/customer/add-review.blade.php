@extends('layouts.customer.app')

@section('content')
<div class="container">
<div class="row">
    
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Review Product</div>
            <div class="panel-body">
            	{!! Form::open(array('route' => 'add-product-rating', 'style' => 'margin-top: 10px;')) !!}
            	  <input type="hidden" name="product_id" value="{{$product_id}}">
	              <div class="form-group">
	                <label for="rating">Choose your rating for this product, 1 - 5 stars:</label>
	                <select name="rating" style="width: 250px;" required>
	                	<option value="">-- Please select a star rating --</option>
	                	<option value="1">1 Star (Low)</option>
	                	<option value="2">2 Stars</option>
	                	<option value="3">3 Stars</option>
	                	<option value="4">4 Stars</option>
	                	<option value="5">5 Stars (High)</option>
	                </select>
	              </div>
	              <div class="form-group">
	                <label for="Review">Review</label><br/>
	                <textarea id="summernote" name="review" required></textarea>
	              </div>
	              <button>Go</button>

	            {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 300
      });
    });

</script>
@endsection