@extends('layouts.customer.app')

@section('content')
<div class="container">
<div class="row">
    
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Your Address Book</div>
            <div class="panel-body">
            	<button><a href="/customer/address/add">Add New Address</a></button>
		        <table class="table table-hover" style="margin-top: 15px;"> 
		            <thead> 
		                <tr> 
		                	<th>#</th>
		                    <th>Address 1</th>
		                    <th>Address 2</th>
		                    <th>Town</th>
		                    <th>Postcode</th>
		                    <th>Created At</th>
		                </tr> 
		            </thead> 
		            <tbody> 
		                @foreach($addresses as $address)
		                <tr> 
		                    <td>{{$address->id}}</td>
		                    <td>{{$address->address1}}</td> 
		                    <td>{{$address->address2}}</td>
		                    <td>{{$address->address_town}}</td>
		                    <td>{{$address->address_postcode}}</td>
		                    <td>{{ date('d-m-Y H:i:s', strtotime($address->created_at)) }} </td>
		                </tr> 
		                @endforeach
		            </tbody> 
		        </table>
            </div>
        </div>
    </div>
</div>
</div>
@endsection