@extends('layouts.customer.app')

@section('content')
<div class="container">
<div class="row">
    
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">Your pallet tracking</div>
            <div class="panel-body">
                <iframe src="https://members.u-p-n.co.uk/podtracker.aspx?con={{$con}}&pcode={{$postcode}}&ButtonName=Search" width="100%" height="600px"></iframe>
            </div>
        </div>
    </div>
</div>
</div>
@endsection