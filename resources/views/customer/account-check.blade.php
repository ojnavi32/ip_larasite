<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
</head>
<body>
	@include('includes.header')

	<div class="container-fluid contact-header">
		<div class="container-fluid contact-overlay">
			<div class="container contact-header-text">
				<div class="row">
					<span class="breadcrumbs-active">home > </span><span class="breadcrumbs"> checkout</span>
				</div>
				<div class="row">
					<div class="contact col-md-3 col-centered bottom-shadow">
						<h1>Checkout</h1>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container product search">
		@if(session('Good'))
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-success">
	                {{ session('Good') }}
	            </div>
	        </div>
		</div>
		@endif

		<div class="row product-data">
			<div class="col-md-12 checkout-account">
				@if($customer->credit_account == 1 && $customer->action == "Account is Current")
					<h1>Payment - Account or Paypal</h1>
					<p>It looks like you have an account with us, would you like to pay for this now or add it to your monthly account?</p>
				@else
					<h1>Payment</h1>
				@endif
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="NameOfBusiness">Would you like us to ship this order to a different address? If so please select it from the dropdown box below. If you need to add an address you can do so <a href="/customer/address" target="_blank">here</a>.</label>
							{{ Form::select('address', $address) }}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="NameOfBusiness">Freeform Box (Useful information for us).</label>
							<textarea class="form-control" rows="3" style="border: 1px solid #d6e2ea; border-radius: 5px;"></textarea>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						@if($customer->credit_account == 1 && $customer->action == "Account is Current")		
							{!! Form::open(array('route' => 'checkout', 'class' => 'cart-form', 'method' => 'get')) !!}
							{!! Form::hidden('pay_type', '1') !!}
							<button class="paypal">Pay via PayPal</button>
							{!! Form::close() !!} 
							{!! Form::open(array('route' => 'checkout', 'class' => 'cart-form', 'method' => 'get')) !!}
							{!! Form::hidden('pay_type', '2') !!}
							<button class="account">Pay on Account</button>
							{!! Form::close() !!}
						@else
							{!! Form::open(array('route' => 'checkout', 'class' => 'cart-form', 'method' => 'get')) !!}
							{!! Form::hidden('pay_type', '1') !!}
							<button class="paypal">Pay via PayPal</button>
							{!! Form::close() !!} 
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">

		$( ".paypal" ).click(function() {
			$('.paypal').html('Please Wait');
		});

		$( ".account" ).click(function() {
			$('.account').html('Please Wait');
		});

		$(function() {
		    $('.product-list-data').matchHeight();
		});
	</script>	

	@include('includes.footer')
</body>
</html>