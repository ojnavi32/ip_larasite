<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
</head>
<body>
	@include('includes.header')

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Our Products</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container product search">
		<div class="row">
			<div class="col-md-5 searchbox">
				{!! Form::open(array('url' => '/search')) !!}
					<input type="text" name="search" placeholder="Search"><button><i class="fa fa-search" aria-hidden="true"></i></button>
				{!! Form::close() !!}
			</div>
		</div>
		
		@if(session('Added'))
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-success">
	                {{ session('Added') }}
	            </div>
	        </div>
		</div>
		@endif

		@if(session('Comparison'))
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-success">
	                Product has been added for comparison, you can view the table <a href="/comparison-view/">here</a>
	            </div>
	        </div>
		</div>
		@endif

		@if(session('Error'))
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-warning">
	                {{ session('Error') }}
	            </div>
	        </div>
		</div>
		@endif

		<div class="row product-data">
			<div class="col-md-12 comparison">
				<h1>Product Comparison</h1>
				<p>Here are the products that you've added to the comparison table</p>
			</div>
			@foreach($comparisons as $comparison)
				<div class="col-md-3">
					<div class="comparisons">
						<div class="comparison-head-image">
							<img src="/images/assets/fullsize/{{$comparison->product_id}}.jpg" class="img-responsive">
							<h2>{{ucwords($comparison->variety)}}</h2>
						</div>
						<div class="comparison-body-text">
							<span>Description:</span> {!! $comparison->prod_desc !!}
						</div>
						<div class="comparison-stock">
							<span>Stock:</span> <strong>{{ $comparison->stock }}</strong>
						</div>
						<div class="comparison-tray">
							<span>Tray Quantity:</span> <strong>{{ $comparison->tray_quantity }}</strong>
						</div>
						<div class="comparison-state">
							<span>Item State:</span> <strong>{{ $comparison->state or 'N/A' }}</strong>
						</div>
						{!! Form::open(array('route' => 'add-to-cart')) !!}
						{!! Form::hidden('product_id', $comparison->product_id) !!}
						{!! Form::hidden('name', $comparison->variety) !!}
							<div class="col-md-12 product-select">
								<div class="row">
									<input type="text" name="qty" value="1">
									Choose Qty
								</div>
							</div>
							<button>Add to Cart</button>
						{!! Form::close() !!}
					</div>
				</div>
			@endforeach
		</div>
	</div>

	@include('includes.footer')
</body>
</html>