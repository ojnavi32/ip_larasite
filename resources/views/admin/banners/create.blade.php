@extends('layouts.admin.app')

@section('content')
<div class="container">
<div class="row">
    @include('admin.banners.layout-banner')
    <div class="col-sm-9">
        
        @if (session('Added'))
            <div class="alert alert-success">
                {{ session('Added') }}
            </div>
        @endif

            <div class="panel panel-default">
                <div class="panel-heading">Add New Banner</div>
                {!! Form::open(array('url' => 'admin/banners/', 'files' => true)) !!}
                <div class="panel-body">
                    <div class="col-md-12">
                        <label for="inputTitle">Name</label>
                        <input type="text" name="name" id="inputTitle" class="form-control" placeholder="Header Image" required="" autofocus="">
                    </div>

                    <div class="col-md-12">
                        <label for="inputMetaTitle">Location</label>
                        <select class="form-control" name="location">
                            <option value="1">Header</option>
                            <option value="2">Home - Middle</option>
                            <option value="3">Footer</option>           
                        </select>
                    </div>

                    <div class="col-md-12">
                        <label for="banner_image">Banner Image</label>
                        <input type="file" name="image" id="banner_image">
                    </div>

                    <div class="col-md-12" style="margin-top: 20px;">
                        <input type="submit" name="Create" value="Add">
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
@endsection
