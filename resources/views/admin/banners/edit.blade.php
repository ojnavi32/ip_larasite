@extends('layouts.admin.app')

@section('content')
<div class="container">
<div class="row">
    @include('admin.banners.layout-banner')
    <div class="col-sm-9">
        
        @if (session('Updated'))
            <div class="alert alert-success">
                {{ session('Updated') }}
            </div>
        @endif

            <div class="panel panel-default">
                <div class="panel-heading">Edit Banner</div>
				{!! Form::open(array('route' => array('admin.banners.update', $banner->id), 'method' => 'put', 'files' => true)) !!}
                <div class="panel-body">
                    <div class="col-md-12">
                        <label for="inputTitle">Name</label>
                        <input type="text" name="name" value="{{$banner->name}}" id="inputTitle" class="form-control" placeholder="Header Image" required="" autofocus="">
                    </div>

                    <div class="col-md-12">
                        <label for="inputMetaTitle">Location</label>
						{{ Form::select('location', array('1' => 'Header', '2' => 'Home - Middle', '3' => 'Footer'), $banner->location, array('class' => 'form-control')) }}
                    </div>
   					<div class="col-md-12">
   						<img style="margin: 20px 0 20px 0;" src="/images/banners/{{$banner->file_location}}">
   						<label>Delete Image</label>
   						<input type="checkbox" name="delete_image">
   					</div>
                    <div class="col-md-12">
                        <label for="banner_image">Banner Image</label>
                        <input type="file" name="image" id="banner_image">
                    </div>
                    <div class="col-md-12" style="margin-top: 20px;">
                        <input type="submit" name="Create" value="Add">
                    </div>
                </div>
            </div>
    </div>
</div>
</div>
@endsection
