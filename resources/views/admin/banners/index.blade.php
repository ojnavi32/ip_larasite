@extends('layouts.admin.app')

@section('content')
<div class="container">
<div class="row">
    @include('admin.banners.layout-banner')
    <div class="col-sm-9">
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>#</th> 
                    <th>Name</th>
                    <th>Image</th>
                    <th>Created At</th>
                    <th>Actions</th> 
                </tr> 
            </thead> 
            <tbody> 
                @foreach($banners as $banner)
                <tr> 
                    <th scope="row">{{$banner->id}}</th> 
                    <td>{{$banner->name}}</td>
                    <td><img class="img-responsive" src="/images/banners/{{$banner->file_location}}"> 
                    <td>{{ date('d-m-Y H:i:s', strtotime($banner->created_at)) }} </td>
                    <td>{!! link_to_route('admin.banners.edit', 'Edit', [$banner->id]) !!} / Delete</td>
                </tr> 
                @endforeach
            </tbody> 
        </table>
    </div>
</div>
</div>
@endsection
