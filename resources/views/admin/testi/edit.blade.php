@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        @if (session('Updated'))
            <div class="alert alert-success">
                {{ session('Updated') }}
            </div>
        @endif

            <div class="panel panel-default">
                <div class="panel-heading">Edit Testimonial</div>
				{!! Form::open(array('route' => array('admin.testi.update', $testimonial->id), 'method' => 'put')) !!}
                <div class="panel-body">
                    <h1></h1>
                    <div class="col-md-12">
                        <label for="inputTitle">Customer Name</label>
                        <input type="text" name="customer_name" value="{{$testimonial->customer_name}}" id="inputTitle" class="form-control" placeholder="Joe Blogs" required="" autofocus="">
                    </div>
                    
                    <div class="col-md-12">
                        <label>Testimonial</label>
                        <textarea id="summernote" name="content">{{$testimonial->content}}</textarea>
                    </div>

                    <input type="submit" value="Update">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 300
      });
    });

</script>
@endsection
