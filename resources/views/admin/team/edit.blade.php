@extends('layouts.admin.app')

@section('content')
<div class="container">
<div class="row">
    @include('admin.team.layout-team')
    <div class="col-sm-9">
        
        @if (session('Updated'))
            <div class="alert alert-success">
                {{ session('Updated') }}
            </div>
        @endif

            <div class="panel panel-default">
                <div class="panel-heading">Edit Team Member</div>
				{!! Form::open(array('route' => array('admin.team.update', $team->id), 'method' => 'put', 'files' => true)) !!}
                <div class="panel-body">
                    <div class="col-md-12">
                        <label for="inputName">Name</label>
                        <input type="text" name="name" id="inputName" value="{{$team->name}}" class="form-control" placeholder="Joe Blogs" required="" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputJobTitle">Job Title</label>
                        <input type="text" name="job_title" value="{{$team->job_title}}" id="inputJobTitle" class="form-control" placeholder="Team Leader" required="" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="summernote">About</label>
                        <textarea id="summernote" name="about">{{$team->about}}</textarea>
                    </div>
                    <div class="col-md-12">
                        <label for="inputTelephone">Telephone Number</label>
                        <input type="text" name="telephone_number" value="{{$team->telephone_number}}" id="inputTelephone" class="form-control" placeholder="0161 124 1214" required="" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputEmail">Email Address</label>
                        <input type="text" name="email_address" value="{{$team->email_address}}" id="inputEmail" class="form-control" placeholder="joe@blogs.com" required="" autofocus="">
                    </div>
                    <div class="col-md-12">
                    	<img src="/images/team/{{$team->image}}" class="img-responsive" style="margin:20px 0;">
                    	<br/>
                        <label for="delete">Delete</label>
						<input type="checkbox" name="delete">
                    </div>
                    <div class="col-md-12">
                        <label for="photo">Upload New Photo</label>
                        <input type="file" name="image" id="photo">
                    </div>
                    
                    <div class="col-md-12" style="margin-top: 20px;">
                        <input type="submit" name="Create" value="Update">
                    </div>
                </div>
            </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 300
      });
    });

</script>

@endsection
