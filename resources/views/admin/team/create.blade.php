@extends('layouts.admin.app')

@section('content')
<div class="container">
<div class="row">
    @include('admin.team.layout-team')
    <div class="col-sm-9">
        
        @if (session('Added'))
            <div class="alert alert-success">
                {{ session('Added') }}
            </div>
        @endif

            <div class="panel panel-default">
                <div class="panel-heading">Add New Team Member</div>
                {!! Form::open(array('url' => 'admin/team/', 'files' => true)) !!}
                <div class="panel-body">
                    <div class="col-md-12">
                        <label for="inputName">Name</label>
                        <input type="text" name="name" id="inputName" class="form-control" placeholder="Joe Blogs" required="" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputJobTitle">Job Title</label>
                        <input type="text" name="job_title" id="inputJobTitle" class="form-control" placeholder="Team Leader" required="" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="summernote">About</label>
                        <textarea id="summernote" name="about"></textarea>
                    </div>
                    <div class="col-md-12">
                        <label for="inputTelephone">Telephone Number</label>
                        <input type="text" name="telephone_number" id="inputTelephone" class="form-control" placeholder="0161 124 1214" required="" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="inputEmail">Email Address</label>
                        <input type="text" name="email_address" id="inputEmail" class="form-control" placeholder="joe@blogs.com" required="" autofocus="">
                    </div>
                    <div class="col-md-12">
                        <label for="photo">Photo</label>
                        <input type="file" name="image" id="photo">
                    </div>
                    
                    <div class="col-md-12" style="margin-top: 20px;">
                        <input type="submit" name="Create" value="Add">
                    </div>
                </div>
            </div>
    </div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 300
      });
    });

</script>

@endsection
