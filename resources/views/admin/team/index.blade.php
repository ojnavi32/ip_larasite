@extends('layouts.admin.app')

@section('content')
<div class="container">
<div class="row">
    @include('admin.team.layout-team')
    <div class="col-sm-9">
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>#</th> 
                    <th>Name</th>
                    <th>Job Title</th>
                    <th>Photo</th>
                    <th>Email Address</th>
                    <th>Created At</th>
                    <th>Actions</th>
                </tr> 
            </thead> 
            <tbody> 
                @foreach($teams as $team)
                <tr> 
                    <th scope="row">{{$team->id}}</th> 
                    <td>{{$team->name}}</td>
                    <td>{{$team->job_title}}</td>
                    <td><img class="img-responsive" src="/images/team/{{$team->image}}"> 
                    <td>{{$team->email_address}}</td>
                    <td>{{ date('d-m-Y H:i:s', strtotime($team->created_at)) }} </td>
                    <td>{!! link_to_route('admin.team.edit', 'Edit', [$team->id]) !!} / Delete</td>
                </tr> 
                @endforeach
            </tbody> 
        </table>
    </div>
</div>
</div>
@endsection
