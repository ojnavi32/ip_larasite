@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        @if (session('Added'))
            <div class="alert alert-success">
                {{ session('Added') }}
            </div>
        @endif

            <div class="panel panel-default">
                <div class="panel-heading">Create New Blog Post</div>
				{!! Form::open(array('url' => 'admin/blog/', 'files' => true)) !!}
                <div class="panel-body">
                    <div class="col-md-12">
                    	<label for="inputTitle">Title</label>
					  	<input type="text" name="title" id="inputTitle" class="form-control" placeholder="Blog Title" required="" autofocus="">
					</div>

                    <div class="col-md-12">
                        <label for="inputMetaTitle">Meta Title</label>
                        <input type="text" name="meta_title" id="inputMetaTitle" class="form-control" placeholder="Meta Title" required="" autofocus="">
                    </div>

                    <div class="col-md-12">
                        <label for="inputMetaDesc">Meta Description</label>
                        <input type="text" name="meta_description" id="inputMetaDesc" class="form-control" placeholder="Meta Description" required="" autofocus="">
                    </div>
 <?php /*?><div class="col-md-12">                    
                        <label for="inputPublishDate">Publish Date</label>
                       
                              <input type="text" name="publish_date" id="publish_date" value="{{ date('d-m-Y') }}" class="form-control" placeholder="Publish Date" required="" autofocus="">                 
                       
                       
                    </div><?php */?>
                    <div class="col-md-12">
                        <label for="photo">Featured Image</label>
                        <input type="file" name="image" id="photo">
                    </div>
					
                    <div class="col-md-12">
                        <label>Blog Content</label>
                        <textarea id="summernote" name="content"></textarea>
					</div>

                    <input type="submit" name="Create">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 300
      });
    });

</script>
 
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <script>
  $(function() {
    $( "#publish_date" ).datepicker({ dateFormat: 'dd-mm-yy' }).val();;
  });
  </script>
@endsection
