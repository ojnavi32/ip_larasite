@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Live Products</div>
                <div class="panel-body">
                    <table class="table table-bordered" id="users-table">
                        <thead>
                            <tr>
                                <th>Product ID</th>
                                <th>Variety</th>
                                <th>State</th>
                                <th>Product Range</th>
                                <th>Stock</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        pageLength: '50',
        ajax: '/datatable/products/all'
    });
</script>
@endsection
