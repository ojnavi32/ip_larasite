@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        @if (session('Added'))
            <div class="alert alert-success">
                {{ session('Added') }}
            </div>
        @endif

            <div class="panel panel-default">
                <div class="panel-heading">Create New Testimonial</div>
				{!! Form::open(array('url' => 'admin/testi/', 'onsubmit' => 'return postForm()')) !!}
                <div class="panel-body">
                    <div class="col-md-12">
                    	<label for="inputTitle">Customer Name</label>
					  	<input type="text" name="customer_name" id="inputTitle" class="form-control" placeholder="Joe Blogs" required="" autofocus="">
					</div>
					
                    <div class="col-md-12">
                        <label>Testimonial</label>
                        <textarea id="summernote" name="content"></textarea>
					</div>

                    <input type="submit" name="Create">
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 300
      });
    });

</script>
@endsection
