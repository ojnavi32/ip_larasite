@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        @if (session('Updated'))
            <div class="alert alert-success">
                {{ session('Updated') }}
            </div>
        @endif

            <div class="panel panel-default">
                <div class="panel-heading">Edit Product</div>
                {!! Form::open(array('route' => array('admin.categories.update', $category->product_category_id), 'method' => 'put')) !!}
                <div class="panel-body">
                    <h1></h1>
                    <div class="col-md-12">Title</label>
                        <input type="text" name="title" id="inputTitle" class="form-control" value="{{$category->title}}" required="" autofocus="">
                    </div>

                    <div class="col-md-12">Visable (1 = Yes, 0 = No)</label>
                        <input type="text" name="visible" id="inputTitle" class="form-control" value="{{$category->visable}}" required="" autofocus="">
                    </div>

                    <div class="col-md-12">Position</label>
                        <input type="text" name="position" id="inputTitle" class="form-control" value="{{$category->position}}" required="" autofocus="">
                    </div>

                    <div class="col-md-12">
                        <input type="submit" name="Create">
                    </div>
                </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 300
      });
    });

</script>
@endsection
