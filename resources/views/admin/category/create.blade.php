@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

        @if (session('Added'))
            <div class="alert alert-success">
                {{ session('Added') }}
            </div>
        @endif

            <div class="panel panel-default">
                <div class="panel-heading">Create New Category</div>
				{!! Form::open(array('url' => 'admin/categories/', 'files' => true)) !!}
                <div class="panel-body">
                    <div class="col-md-12">Title</label>
					  	<input type="text" name="title" id="inputTitle" class="form-control" placeholder="Trees" required="" autofocus="">
					</div>

                    <div class="col-md-12">Visable (1 = Yes, 0 = No)</label>
                        <input type="text" name="visible" id="inputTitle" class="form-control" placeholder="Trees" required="" autofocus="">
                    </div>

                    <div class="col-md-12">Position</label>
                        <input type="text" name="position" id="inputTitle" class="form-control" placeholder="Trees" required="" autofocus="">
                    </div>

                    <div class="col-md-12">Category Image Upload</label>
                        <div class="form-group">
        					<label class="btn btn-default btn-file">
                                Browse <input type="image" style="display: none;">
                            </label>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <input type="submit" name="Create">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



</script>
@endsection
