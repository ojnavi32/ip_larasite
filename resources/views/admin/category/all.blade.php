@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Categories</div>
                <div class="panel-body">
                    <button style="margin-bottom: 20px;"><a href="/admin/categories/create">Add Category</a></button>
                    <table class="table table-bordered" id="users-table">
                        <thead>
                            <tr>
                                <th>Product Category ID</th>
                                <th>Title</th>
                                <th>Image</th>
                                <th>Visable</th>
                                <th>Postion</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        pageLength: '50',
        ajax: '/datatable/categories/all'
    });
</script>
@endsection
