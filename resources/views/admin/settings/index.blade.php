@extends('layouts.admin.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        @if (session('Added'))
            <div class="alert alert-success">
                {{ session('Added') }}
            </div>
        @endif
            <div class="panel panel-default">
                <div class="panel-heading">Site Content</div>
                @if(isset($settings->id))
                	{!! Form::open(array('route' => array('admin.settings.update', $settings->id), 'method' => 'put')) !!}
                @else
                	{!! Form::open(array('url' => 'admin/settings/', 'onsubmit' => 'return postForm()')) !!}
                @endif
                <div class="panel-body">
					<div class="col-md-12">
						<label for="inputTitle">About Us</label>
	                        <textarea id="summernote" name="about_us">{{$settings->about_us or ''}}</textarea>
					</div>
				</div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Site Settings</div>
                @if(isset($settings->id))
                	{!! Form::open(array('route' => array('admin.settings.update', $settings->id), 'method' => 'put')) !!}
                @else
                	{!! Form::open(array('url' => 'admin/settings/', 'onsubmit' => 'return postForm()')) !!}
                @endif
                <div class="panel-body">
					<div class="col-md-12">
						<label for="inputTitle">Telephone Number</label>
	                        <input type="text" name="telephone_number" value="{{$settings->telephone_number or ''}}" id="inputTitle" class="form-control" placeholder="0161 253 1231" required="" autofocus="">
					</div>
					<div class="col-md-12">
						<label for="inputTitle">Fax Number</label>
	                        <input type="text" name="fax_number" value="{{$settings->fax_number or ''}}" id="inputTitle" class="form-control" placeholder="0161 253 1231" required="" autofocus="">
					</div>
					<div class="col-md-12">
						<label for="inputTitle">Email Number</label>
	                        <input type="text" name="email_address" value="{{$settings->email_address or ''}}" id="inputTitle" class="form-control" placeholder="joe@domain.com" required="" autofocus="">
					</div>
				</div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">SEO Settings</div>
                <div class="panel-body">
					<div class="col-md-12">
						<label for="inputTitle">Google UA</label>
	                        <input type="text" name="google_ua" value="{{$settings->google_ua or ''}}" id="inputTitle" class="form-control" placeholder="UA-123112-1" autofocus="">
					</div>
					<div class="col-md-12">
						<label for="inputTitle">Google WMT</label>
	                        <input type="text" name="google_wmt" value="{{$settings->google_wmt or ''}}" id="inputTitle" class="form-control" placeholder="1234567890987654321" autofocus="">
					</div>
					<br/><br/>
				</div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Social Settings</div>
                <div class="panel-body">
					<div class="col-md-12">
						<label for="inputTitle">Twitter</label>
	                        <input type="text" name="twitter" value="{{$settings->twitter or ''}}" id="inputTitle" class="form-control" placeholder="@twitter" autofocus="">
					</div>
					<div class="col-md-12">
						<label for="inputTitle">Facebook</label>
	                        <input type="text" name="facebook" value="{{$settings->facebook or ''}}" id="inputTitle" class="form-control" placeholder="https://www.facebook.com" autofocus="">
					</div>
					<div class="col-md-12">
						<label for="inputTitle">Linkedin</label>
	                        <input type="text" name="linkedin" value="{{$settings->linkedin or ''}}" id="inputTitle" class="form-control" placeholder="https://linkedin.com" autofocus="">
					</div>
					<div class="col-md-12">
						<label for="inputTitle">Google +</label>
	                        <input type="text" name="google_plus" value="{{$settings->google_plus or ''}}" id="inputTitle" class="form-control" placeholder="https://google.com" autofocus="">
					</div>
					<br/><br/>
					<div class="col-md-12" style="margin-top: 20px;">
						@if(isset($settings->id))
			            	<input type="submit" name="Update" value="Update">
			            @else
			            	<input type="submit" name="Save" value="Save">
			            @endif
			        </div>
				</div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
      $('#summernote').summernote({
        height: 300
      });
    });

</script>

@endsection
