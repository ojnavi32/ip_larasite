<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
</head>
<body>
	@include('includes.header')

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Our Products</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container product search">
		@if(session('Good'))
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-success">
	                {{ session('Good') }}
	            </div>
	        </div>
		</div>
		@endif
		<div class="row">
			<div class="col-md-5 searchbox">
				{!! Form::open(array('url' => '/search')) !!}
					<input type="text" name="search" placeholder="Search"><button><i class="fa fa-search" aria-hidden="true"></i></button>
				{!! Form::close() !!}
			</div>
		</div>

		<div class="row product-data">
			<div class="col-md-4 left-menu">
				<ul>
					@foreach($categories as $category)
					<?php $link = str_replace(' ', '-', $category->title); ?>
					<li>
						<img src="/images/assets/{{$category->img_src}}" width="40" height="40">
						<a href="/product/category/{{$link}}">{{$category->title}}</a>
					</li>
					@endforeach
				</ul>
			</div>
			<div class="col-md-8 product-list">
				<div class="row">
					@foreach($products as $product)
						<div class="col-md-4">
							<div class="product-list-data">
								<?php $slug = str_replace(' ', '-', $product->variety); ?>
								<?php $slug = str_replace('/', '-', $slug); ?>
								<a href="/product/{{$product->product_id}}/{{$slug}}/">
								<h1>{{$product->variety}}</h1>
								<div class="product-image">
									<img src="/images/assets/fullsize/{{$product->product_id}}.jpg" class="img-responsive">
								</div>
								<div class="col-md-12">
									<div class="row">
										<button>More Info</button>
									</div>
								</div>
								<div style="clear: both"></div>
								</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
		    $('.product-list-data').matchHeight();
		});
	</script>	

	@include('includes.footer')
</body>
</html>