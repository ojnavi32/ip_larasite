<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
</head>
<body>
	@include('includes.header')

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Your Cart</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container cart">
		<div class="col-md-12">
	
			@if(session('Removed'))
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="row">
				    <div class="alert alert-success">
		                {{ session('Removed') }}
		            </div>
		        </div>
			</div>
			@endif
			@if(session('Good'))
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="row">
				    <div class="alert alert-success">
		                {{ session('Good') }}
		            </div>
		        </div>
			</div>
			@endif
			@if(session('Updated'))
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="row">
				    <div class="alert alert-success">
		                {{ session('Updated') }}
		            </div>
		        </div>
			</div>
			@endif
			@if(session('Error'))
			<div class="col-md-12" style="margin-top: 30px;">
				<div class="row">
				    <div class="alert alert-warning">
		                {{ session('Error') }}
		            </div>
		        </div>
			</div>
			@endif

			<table id="cart" class="table table-hover table-condensed">
				<thead>
					<tr>
						<th style="width:50%">Product</th>
						<th style="width:10%;text-align: center;">Price</th>
						<th style="width:8%;text-align: center;">Quantity</th>
						<th style="width:8%;text-align: center;">Total</th>
						<th style="width:10%"></th>
					</tr>
				</thead>
				<tbody>
					@foreach(Cart::content() as $row)
					<tr>
						<td data-th="Product">
							<div class="row">
								<div class="col-sm-2 hidden-xs"><img src="/images/assets/thmbs/{{$row->id}}.jpg" class="img-responsive"/></div>
								<div class="col-sm-10">
									<h4 class="nomargin">{{ $row->name }}</h4>
									<span class="cart-description">{!! $row->options->size !!}</span>
									<span class="cart-description">{!! $row->options->description !!}</span>
								</div>
							</div>
						</td>
						<td data-th="Price">&pound;{{ number_format($row->price,2)}}</td>
							{!! Form::open(array('route' => 'update-item', 'class' => 'cart-form')) !!}
							{!! Form::hidden('item_id', $row->id) !!}
							{!! Form::hidden('cart_row_id', $row->rowId) !!}
							<td data-th="Quantity">
								<input type="number" name="qty" class="form-control text-center" value="{{$row->qty}}">
							</td>
							<td data-th="Price">&pound;{{ number_format($row->price * $row->qty,2) }}</td>
							<td class="actions" data-th="">
								<button class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button>
							{!! Form::close() !!}	
								{!! Form::open(array('route' => 'remove-item', 'class' => 'cart-form')) !!}
							{!! Form::hidden('cart_row_id', $row->rowId) !!}
								<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>
							{!! Form::close() !!}						
						</td>

					</tr>					
			        @endforeach
				</tbody>
				<tfoot>
					<tr class="visible-xs">
						<td class="text-center"><strong>Total {{ Cart::subtotal() }}</strong></td>
					</tr>
					@if(Cart::subtotal() > 200)
					<tr>
						<td colspan="3"></td>
						<td class="hidden-xs text-center"><strong>Shipping &pound;0.00</strong></td>
						<td colspan="1"></td>
					</tr>
					@else
					<tr>
						<td colspan="3"></td>
						<td class="hidden-xs text-center"><strong>Shipping &pound;9.00</strong></td>
						<td colspan="1"></td>
					</tr>
					@endif
					<tr>
						<td colspan="3"></td>
						<td class="hidden-xs text-center"><strong>Sub Total @if(Cart::subtotal() > 200) &pound;{{Cart::subtotal()}} @else &pound;{{number_format(Cart::subtotal()+9.00,2)}} @endif</strong></td>
						<td colspan="1"></td>
					</tr>
					<tr>
						<td colspan="3"></td>
						<td class="hidden-xs text-center">
						<strong>Vat 
							@if(Cart::subtotal() > 200) 
								&pound;{{Cart::tax()}} 
							@else
								<?php
								$total = Cart::subtotal()+9.00;
								$vat = $total * 0.20;
								$totalIncVat = $vat;
								?>
								&pound;{{number_format($totalIncVat,2)}} 
							@endif
						</strong></td>
						<td colspan="1"></td>
					</tr>
					<tr>
						<td><a href="/product/categories" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
						<td colspan="2" class="hidden-xs"></td>
						<td class="hidden-xs text-center"><strong>Total &pound;@if(Cart::subtotal() > 200) {{Cart::total()}} @else {{number_format($total + $totalIncVat,2)}} @endif</strong></td>
						<td><a href="/cart/check-payment" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>


	@include('includes.footer')
</body>
</html>