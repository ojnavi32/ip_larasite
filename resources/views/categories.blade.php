<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
</head>
<body>
	@include('includes.header')

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Our Products</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container categories">
		@if(session('Good'))
		<div class="col-md-12" style="margin-top: 30px;">
			<div class="row">
			    <div class="alert alert-success">
	                {{ session('Good') }}
	            </div>
	        </div>
		</div>
		@endif
		<div class="row">
			<div class="col-md-12">
				<div class="row categories">
					@foreach($categories as $category)
					<?php $link = str_replace(' ', '-', $category->title); ?>
					<a href="/product/category/{{$link}}">
					<div class="col-md-3 center-text">
						<img src="/images/assets/{{$category->img_src}}">						
						<h4>{{$category->title}}</h4>
					</div></a>
					@endforeach
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function() {
		    $('.product-list-data').matchHeight();
		});
	</script>	

	@include('includes.footer')
</body>
</html>