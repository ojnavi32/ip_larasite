<html>
<head></head>
<body>
<h1>{{$title}}</h1>

<p>Customer: {{$customer->company}}<br/>
Name: {{$customer->fname}}<br/>
Phone: {{$customer->phone_office}}<br/>
Email: {{$customer->email}}<br/>
</p>

<p>Order No: <strong>{{$order->orders_id}}</strong></p>
<p>Payment Type: {{$order->payment_type}}</p>

<p>Items:</p>
<ul>
	@foreach($products as $product)
		<li>Product: {{ $product->variety }} - Qty: {{ $product->qty }} - Price: &pound;{{ number_format($product->price,2) }}</li>	
	@endforeach
</ul>
<p><strong>Order Value:</strong> &pound;{{ number_format($order->payment_gross) }}</p>
<p>{{ $sale_message }}</p>

</body>
</html>