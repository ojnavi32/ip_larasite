<!DOCTYPE html>
<html>
<head>
	@include('includes.head')
</head>
<body>
	@include('includes.header')

	<div class="container-fluid contact-overlay">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5 pull-left breadcrumb-left">
					<h2>We are here to supply goods and service, today, tomorrow and whenever you require them...</h2>
				</div>
				<div class="contact col-xs-12 col-md-2 col-centered bottom-shadow"><h1>Testimonials</h1></div>
				<div class="col-xs-12 col-md-5 pull-right breadcrumb-right">
					<h2>This includes next day delivery service nationally from our extensive range of products!</h2>
				</div>
			</div>
		</div>
	</div>
	
	<div class="container testimonials">
		<div class="row">
			@foreach($testimonials as $testimonial)
				<div class="col-md-4">
					<blockquote>
						<p>{!! $testimonial->content !!}</p>
						<h3>{{$testimonial->customer_name}}</h3>
					</blockquote>
				</div>
			@endforeach
		</div>
	</div>

	</script>	

	@include('includes.footer')
</body>
</html>