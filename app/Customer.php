<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable
{
    protected $table = 'customer';
    protected $primaryKey = 'customer_id';
    public $timestamps = false;

    public function getAddresses() {
    	return $this->hasMany('JFH\CustomerAddress', 'customer_id', 'customer_id');
    }
}
