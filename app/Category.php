<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'product_category_id';
    public $timestamps = false;
    protected $fillable = 
    	['product_category_id',
    	'title',
    	'img_src',
    	'visible',
    	'position',];
}