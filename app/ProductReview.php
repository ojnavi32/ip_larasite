<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    protected $table = 'product_review';

    protected $fillable = [
		'customer_id',
		'product_id',
		'rating',
		'review'
	];

    public function getReviewer() {
    	return $this->hasOne('JFH\Customer', 'customer_id', 'customer_id');
    }

    public function getRating() {
    	return round($this->avg('rating'));
    }
}