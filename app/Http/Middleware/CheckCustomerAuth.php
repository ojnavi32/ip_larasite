<?php

namespace JFH\Http\Middleware;

use Closure, Redirect;
use JFH\Helpers;

class CheckCustomerAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty(Helpers::IsUserLoggedIn())) {
            return Redirect::route('customer-index');
        } else {
            return 'hello';
            return $next($request);
        }        
    }
}
