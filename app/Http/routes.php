<?php

use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index')->name('main-home');
Route::get('/contact-us', 'ContactController@index');
Route::post('/contact-us/store', 'ContactController@StoreMessage')->name('contact-store');
Route::get('/product/category/{slug}', 'ProductController@categories')->name('load-categories');
Route::get('/product/{id}/{slug}', ['as' => 'get-product', 'uses' => 'ProductController@ProductGet']);

Route::get('/product/categories', ['as' => 'get-categories', 'uses' => 'CategoryController@index']);

Route::post('/product/update', ['as' => 'product-update', 'uses' => 'ProductController@ProductUpdatesGet']);
Route::post('/product/update/break', ['as' => 'product-update-break', 'uses' => 'ProductController@ProductUpdatesBreakGet']);
Route::post('/product/comparison', 'ProductController@ProductComparison')->name('product-comparison');
Route::get('/comparison-view/', 'ProductController@ViewProductComparison')->name('view-product-comparison');
Route::get('/terms', 'MiscController@ViewTerms')->name('view-terms');
Route::get('/privacy', 'MiscController@ViewPrivacy')->name('view-privacy');

Route::get('/search/{search}', 'SearchController@searchquery')->name('searchquery');
Route::post('/search', 'SearchController@search')->name('search');

Route::get('/blog', 'FrontBlogController@index')->name('all-blog-posts');

Route::get('/testimonials', 'HomeController@testi')->name('testimonials');

Route::group(['prefix' => 'customer'], function () {
  Route::get('/', 'Customer\CustomerController@index')->name('customer-index');
  Route::post('/register', 'Customer\CustomerController@register')->name('customer-register');
  Route::get('/register/thanks/', 'Customer\CustomerController@thanks')->name('customer-register-thanks');
  Route::post('/login', 'Customer\CustomerController@login')->name('customer-login');
  Route::get('/home', 'Customer\CustomerController@home')->name('customer-home');
  Route::get('/logout', 'Customer\CustomerController@logout')->name('customer-logout');
  Route::get('/orders', 'Customer\CustomerController@orders')->name('customer-orders');
  Route::get('/orders/view/{id}', 'Customer\CustomerController@OrdersView')->name('customer-orders-view');
  Route::get('/credit-application', 'Customer\CustomerController@GetCreditApplication')->name('view-credit-application');
  Route::post('/credit-application/submit', 'Customer\CustomerController@SubmitCreditApplication')->name('submit-credit-application');
  Route::get('/orders/view/pdf/{id}', 'Customer\CustomerController@viewPdf')->name('customer-order-view-pdf');
  Route::get('/track', 'Customer\CustomerController@trackOrder')->name('track-order');
  Route::post('/track/pallet/', 'Customer\CustomerController@trackOrderPallet')->name('track-order-pallet');
  Route::get('/getaddresslist', 'Customer\CustomerController@getAddress')->name('get-address-list');
  Route::get('/address', 'Customer\CustomerController@viewAddresses')->name('view-address');
  Route::get('/address/add', 'Customer\CustomerController@addAddresses')->name('add-address');
  Route::post('/address/store', 'Customer\CustomerController@storeAddress')->name('store-address');
  Route::get('/rating/{id}', 'Customer\CustomerController@viewRating')->name('view-product-rating');
  Route::post('/rating/add', 'Customer\CustomerController@addRating')->name('add-product-rating');
  Route::post('/quick-register', 'Customer\CustomerController@quickRegistration')->name('customer-register-quick');
});

Route::group(['prefix' => 'cart'], function () {
  Route::post('/product/add-to-cart', 'ProductController@AddToCart')->name('add-to-cart');
  Route::get('/', 'CartController@index')->name('showcart');
  Route::post('/remove/item', 'CartController@removeItem')->name('remove-item');
  Route::post('/update/item', 'CartController@updateItem')->name('update-item');
  Route::get('/checkout', 'CartController@Checkout')->name('checkout');
  Route::get('/checkout/callback/', 'CartController@Callback')->name('checkout-callback');
  Route::get('/check-payment', 'CartController@CheckPayment')->name('customer-check-payment');
});

Route::post('/contact/callback', 'MiscController@ContactPost')->name('callback-form');
Route::post('/contact/form', 'MiscController@ContactFromPost')->name('form-contact');



Route::auth();

Route::group(['middleware' => 'auth'], function () {
    
    //Admin Dashboard
    Route::get('/admin', 'AdminController@index');
	
    // Blog Resource
   Route::resource('/admin/blog', 'BlogController');
    // Testimonials Resource
   Route::resource('/admin/testi', 'TestiController');
    // Settings Resource
   Route::resource('/admin/settings', 'admin\SettingsController');
    // Manage Banners Resource
   Route::resource('/admin/banners', 'admin\BannerController');
    // Manage Team Resource
   Route::resource('/admin/team', 'admin\TeamController');
    // Manage Products
   Route::resource('/admin/products', 'admin\ProductController');
    // Manage Categories
   Route::resource('/admin/categories', 'admin\CategoryController');

});

  //Datatabels
  Route::get('/datatable/products/all', 'admin\ProductController@getAllProducts');
  Route::get('/datatable/categories/all', 'admin\CategoryController@getAllCategories');

  Route::get('/pp', 'ProductController@CreatePayPal')->name('create-paypal');
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});