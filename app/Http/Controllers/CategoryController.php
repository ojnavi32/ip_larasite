<?php

namespace JFH\Http\Controllers;

use JFH\Product, JFH\Category;
use JFH\Helpers, View, Input, Redirect, Cart, SEOMeta;
use Illuminate\Http\Request;
use JFH\Http\Requests;

class CategoryController extends Controller {
    
	public function index() {
		SEOMeta::setTitle('Categories | Garden Supplies Cheshire | JFH horticultural');
        SEOMeta::setDescription('This is my page description');
		$viewData['categories'] = Category::where('visible', 1)->get();
		return View::make('categories', $viewData);
		
	}

}
