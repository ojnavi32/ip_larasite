<?php

namespace JFH\Http\Controllers;

use JFH\Product, JFH\Category, JFH\Orders, JFH\Customer, JFH\OrderItems;
use JFH\Helpers, View, Input, Redirect, Cart, SEOMeta, Carbon\Carbon, Session, DB;
use Illuminate\Http\Request;
use JFH\Http\Requests;
use Mail;

use PayPal\Api\Amount; 
use PayPal\Api\Details; 
use PayPal\Api\Item; 
use PayPal\Api\ItemList; 
use PayPal\Api\Payer; 
use PayPal\Api\Payment;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls; 
use PayPal\Api\Transaction;

class CartController extends Controller {
   
	public function index() {
		SEOMeta::setTitle('My Cart | Garden Supplies Cheshire | JFH horticultural');
        SEOMeta::setDescription('This is my page description');
		return View::make('cart');

	}

	public function removeItem() {

		Cart::remove(Input::get('cart_row_id'));
		return Redirect::back()->with('Removed', 'Item has been removed from you\'re basket.');

	}

	public function updateItem() {

		$product = Product::where('product_id', Input::get('item_id'))->first();

		//Get Price Break Information
		$break_a = $product->break_a;
		$break_b = $product->break_b ?: '1000000';
		$break_c = $product->break_c ?: '1000000';
		$break_d = $product->break_d ?: '1000000';
		$break_e = $product->break_e ?: '1000000';

		//Work out product price
		$qty = Input::get('qty');

		if(empty($break_b)) {

			$price = $product->price_a;

		} else {

			if($qty >= $break_a && $qty < $break_b) {
				$price = $product->price_a;
			}
			if($qty >= $break_b && $qty < $break_c) {
				$price = $product->price_b;
			}
			if($qty >= $break_c && $qty < $break_d) {
				$price = $product->price_c;
			}
			if($qty >= $break_d && $qty < $break_e) {
				$price = $product->price_d;
			}
			if($qty >= $break_e) {
				$price = $product->break_e;
			}
		}
		
		Cart::update(Input::get('cart_row_id'), ['qty' => Input::get('qty'), 'price' => $price]);

		return Redirect::back()->with('Updated', 'Item has been updated.');
	}

	public function CheckPayment() {

		$viewData['customer'] = Customer::where('customer_id', Session('customer_id'))->first();
		$viewData['address'] = DB::table('customer_address')->where('customer_id', Session('customer_id'))->lists('address1', 'id');
		return View::make('customer.account-check', $viewData);


	}

	public function checkout(Request $request) {

		if($request->pay_type == 1) {

			if(Cart::count()) {

				$order_state = Orders::find(Session::get('order_id'));
				$order_state->order_taken = 7;
				$order_state->save();

				$apiContext = new \PayPal\Rest\ApiContext(
			    new \PayPal\Auth\OAuthTokenCredential(
			        env('CLIENT_ID', null),     // ClientID
			        env('SECRET_KEY', null)      // ClientSecret
			    	)
				);
				
			    $apiContext->setConfig(
			        array(
			            'mode' => 'live',
			        )
			    );

				$payer = new Payer(); 
				$payer->setPaymentMethod("paypal");

				$subtotal = str_replace(',', '', cart::subtotal());

				//work out tax
				if(Cart::subtotal() > 200) {
					$tax = str_replace(',', '', cart::tax());
					$total = cart::subtotal();
					$shipping = 0.00;
				} else {
					$shipping = 9.00;
					$total = Cart::subtotal()+$shipping;
					$vat = $total * 0.20;
					$tax = $vat;	
				}

				$total = $total+$tax;

				$item  = array();
				$items = array();
				$index = 0;
				foreach (Cart::content() as $row) {

					$product = Product::find($row->id);

					$orderItems = new OrderItems;
					$orderItems->orders_id = Session::get('order_id');
					$orderItems->product_id = $product->product_id;
					$orderItems->qty = $row->qty;
					$orderItems->tray_quantity = $product->tray_quantity;
					$orderItems->variety = $product->variety;
					$orderItems->pot_size = $product->pot_size;
					$orderItems->price = $row->price;
					$orderItems->state = $product->state;
					$orderItems->form = $product->form;
					$orderItems->vatcode = 'S';
					$orderItems->product_range = $product->product_range;

					//Get Price Break Information
					$break_a = $product->break_a;
					$break_b = $product->break_b ?: '1000000';
					$break_c = $product->break_c ?: '1000000';
					$break_d = $product->break_d ?: '1000000';
					$break_e = $product->break_e ?: '1000000';

					//Work out product price
					$qty = $row->qty;


					if(empty($break_b)) {

						$desc = $product->break_a_desc;
						$price = $product->price_a;

					} else {

						if($qty >= $break_a && $qty < $break_b) {
							$desc = $product->break_a_desc;
							$price = $product->price_a;
						}
						if($qty >= $break_b && $qty < $break_c) {
							$desc = $product->break_b_desc;
							$price = $product->price_b;
						}
						if($qty >= $break_c && $qty < $break_d) {
							$desc = $product->break_c_desc;
							$price = $product->price_c;
						}
						if($qty >= $break_d && $qty < $break_e) {
							$desc = $product->break_d_desc;
							$price = $product->price_d;
						}
						if($qty >= $break_e) {
							$desc = $product->break_e_desc;
							$price = $product->price_e;
						}
					}

					$orderItems->price_break_desc = $desc;
					$orderItems->barcode = $product->barcode;
					$orderItems->save();

					$index++;
					$item[$index] = new Item(); 
					$item[$index]->setName($row->name) 
					->setCurrency('GBP') 
					->setQuantity($row->qty) 
					->setSku($row->rowId) 
					->setPrice($price);

					$items[] = $item[$index];
				}

				$itemList = new ItemList();
				$itemList->setItems($items);

				$details = new Details(); 
				$details->setTax($tax)
				->setSubtotal($subtotal)
				->setShipping($shipping);

				$amount = new Amount(); 
				$amount->setCurrency("GBP")
				->setTotal($total)
				->setDetails($details);
				
				$transaction = new Transaction(); 
				$transaction->setAmount($amount) 
				->setItemList($itemList) 
				->setDescription("Payment description") 
				->setInvoiceNumber(uniqid());

				$redirectUrls = new RedirectUrls(); 
				$redirectUrls->setReturnUrl(env('RETURN_URL', null)) 
				->setCancelUrl(env('CANCEL_URL', null));

				$payment = new Payment(); 
				$payment->setIntent("sale") 
				->setPayer($payer) 
				->setRedirectUrls($redirectUrls) 
				->setTransactions(array($transaction));

				$request = clone $payment;

				try { 
					$payment->create($apiContext); 
				} catch (\PayPal\Exception\PayPalConnectionException $e) {
				        echo $e->getData();
				        // This should give you a json object explaining what exactly is causing that 400 exception.
				        exit(1);

				} catch (\Exception $e) {
				        echo $e->getMessage();
				        exit(1);

				}

				$approvalUrl = $payment->getApprovalLink();
				return Redirect::away($approvalUrl);

			} else {
				return Redirect::back()->with('Error', 'You don\'t have any products in your basket, please add some and try again.');
			}

		} else {

			$customer_email = Customer::where('customer_id', Session('customer_id'))->pluck('email');

			$order = new Orders;
			$order->customer_id = Session('customer_id');
			$order->timedate = Carbon::now();
			$order->payment_email = $customer_email;
			$order->vat_code = 'T1';
			$order->dateoforder = Carbon::now();
			$order->guid = rand();
			$order->payment_type = 'other';
			$order->payment_gross = cart::subtotal();
			$order->vat_charged = cart::tax();
			$order->order_taken = '1';
			$order->save();

			foreach (Cart::content() as $row) {
				$product = Product::find($row->id);

				$orderItems = new OrderItems;
				$orderItems->orders_id = $order->orders_id;
				$orderItems->product_id = $product->product_id;
				$orderItems->qty = $row->qty;
				$orderItems->tray_quantity = $product->tray_quantity;
				$orderItems->variety = $product->variety;
				$orderItems->pot_size = $product->pot_size;
				$orderItems->price = $row->price;
				$orderItems->state = $product->state;
				$orderItems->form = $product->form;
				$orderItems->vatcode = 'S';
				$orderItems->product_range = $product->product_range;

				//Get Price Break Information
				$break_a = $product->break_a;
				$break_b = $product->break_b;
				$break_c = $product->break_c;
				$break_d = $product->break_d;
				$break_e = $product->break_e;

				//Work out product price
				$qty = $row->qty;

				if($qty <= $break_a){
					$desc = $product->break_a_desc;
				} elseif ($qty <=  $break_b) {
					$desc = $product->break_b_desc;
				} elseif ($qty <=  $break_c) {
					$desc = $product->break_c_desc;
				} elseif ($qty <=  $break_d) {
					$desc = $product->break_d_desc;
				} elseif ($qty <=  $break_e) {
					$desc = $product->break_e_desc;
				} else {
					$desc = $product->break_a_desc;
				}

				$orderItems->price_break_desc = $desc;
				$orderItems->barcode = $product->barcode;
				$orderItems->save();
			}

			$emailData['title'] = 'JFH - Order';
			$emailData['customer'] = Customer::find(Session::get('customer_id'));
			$emailData['sale_message'] = 'Thanks for your order, your order can be viewed online with your account.';
		    $emailData['products'] = OrderItems::where('orders_id', $order->orders_id)->get();
			$emailData['order'] = Orders::where('orders_id', $order->orders_id)->first();

			$data = array(
				'customer_email' => $emailData['customer']->email,
			);

	        Mail::send('email.sale', $emailData, function ($message) use ($data)
	        {
	            $message->from('website@jfhhorticultural.com', 'Sale on Website');
	            $message->to($data['customer_email']);
	            $message->cc('katrina@jfhhorticultural.com');
	            //$message->to('scott@firstinternet.co.uk');
	        });
	        
			Cart::destroy();

			return redirect()->route('customer-orders-view', ['id' => $order->orders_id])->with('Ok', 'Your order has been placed, please see below for confirmation.');

		}

	}

	public function Callback(Request $request) {

		if($request->get('success') == 'true') {

			$apiContext = new \PayPal\Rest\ApiContext(
		    new \PayPal\Auth\OAuthTokenCredential(
		        env('CLIENT_ID', 'AUqs2A1E329mPc52DInKJnU6FGLXu7Bdzxw7wOilOR0U5jPJHqMejksDtEuHAV1Ggklz5N2a_7qnMu1_'),     // ClientID
		        env('SECRET_KEY', 'EM2IbcZQC4BlMqvyhwHnpVNq1bvYbXa7RqcWfHbWL1saX6Z1dgPAik1Xw8m8TVLgHeHVgXXjED7swwsI')      // ClientSecret
		    	)
			);

            $apiContext->setConfig(
                array(
                    'mode' => 'live',
                )
            );

			$paymentId = $request->get('paymentId');
	    	$payment = Payment::get($paymentId, $apiContext);

			$execution = new PaymentExecution();
			$execution->setPayerId($request->get('PayerID'));

			$result = $payment->execute($execution, $apiContext);

	
			//return Session::get('order_id');
			//update orders
			$order_id = \DB::table('orders')->orderBy('dateoforder', 'desc')->first();
			$order = Orders::find($order_id->orders_id);
			$order->payment_status = 'completed';
			$order->transaction_id = $request->get('paymentId');
			$order->order_taken = '8';
			$order->save();

	        
			$emailData['title'] = 'JFH - Order';
			$emailData['customer'] = Customer::find(Session::get('customer_id'));
			$emailData['sale_message'] = 'Thanks for your order, your order can be viewed online with your account.';
		    $emailData['products'] = OrderItems::where('orders_id', Session::get('order_id'))->get();
			$emailData['order'] = Orders::where('orders_id', Session::get('order_id'))->first();

			$data = array(
				'customer_email' => $emailData['customer']->email,
			);

	        Mail::send('email.sale', $emailData, function ($message) use ($data)
	        {
	            $message->from('website@firstinternet.co.uk', 'Sale on Website');
	            $message->to($data['customer_email']);
	            $message->cc('katrina@jfhhorticultural.com');
	            //$message->to('scott@firstinternet.co.uk');
	        });

			Cart::destroy();

			return redirect()->route('customer-orders-view', ['id' => $order->orders_id])->with('Ok', 'Your order has been placed, please see below for confirmation.');

		} else {

			$order_id = \DB::table('orders')->orderBy('dateoforder', 'desc')->first();
			$order = Orders::find($order_id->orders_id);
			$order->order_taken = 7;
			$order->save();
			return redirect()->route('showcart')->with('Error', 'Your checkout wasn\'t successful, please check your details and try again.');
		}
	}
}
