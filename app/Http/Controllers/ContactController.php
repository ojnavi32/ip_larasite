<?php

namespace JFH\Http\Controllers;

use JFH\ContactForm;
use JFH\Helpers, View, Input, Redirect, SEOMeta, Mail;
use Illuminate\Http\Request;
use JFH\Http\Requests;

class ContactController extends Controller {


	public function index() {
		SEOMeta::setTitle('Contact Us | Garden Supplies Cheshire | JFH horticultural');
        SEOMeta::setDescription('This is my page description');
		return View::make('contact');

	}

	public function StoreMessage() {
			
		$contact = new ContactForm;
		$contact->name = Input::get('name');
		$contact->email_address = Input::get('email');
		$contact->budget = Input::get('number');
		$contact->subject = Input::get('subject');
		$contact->message = Input::get('message');
		$contact->save();

		$emailData['email'] = Input::get('email');
        $emailData['name'] = Input::get('name');
        $emailData['number'] = Input::get('number');
        $emailData['message_data'] = Input::get('message');
        $emailData['title'] = 'Contact Form';

        Mail::send('email.contact', $emailData, function ($message)
        {
            $message->from('website@firstinternet.co.uk', 'JFH Website');
            //$message->to('katrina@jfhhorticultural.com');
            $message->bcc('scott@firstinternet.co.uk');
        });


		return Redirect::back()->with('Ok', 'Your message has been submitted, we\'ll contact you as soon as possile.');

	}


}
