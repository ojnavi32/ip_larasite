<?php

namespace JFH\Http\Controllers;

use JFH\Product, JFH\Category, JFH\RecentlyViewed, JFH\ComparisonTable, JFH\Customer, JFH\Orders;
use JFH\Helpers, View, Input, Redirect, Cart, DB, SEOMeta, Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use JFH\Http\Requests;

use PayPal\Api\Amount; 
use PayPal\Api\Details; 
use PayPal\Api\Item; 
use PayPal\Api\ItemList; 
use PayPal\Api\Payer; 
use PayPal\Api\Payment; 
use PayPal\Api\RedirectUrls; 
use PayPal\Api\Transaction;

class ProductController extends Controller {

	public function index() {

		return Category::where('visible', 1)->get();

	}

	public function categories($slug) {
		$slug = str_replace('-', ' ', $slug);
		SEOMeta::setTitle(''.$slug.' | JFH Horticultural Supplies Cheshire');
        SEOMeta::setDescription('This is my page description');
		$viewData['products'] = Product::where('catagory', $slug)->where('recomended', 1)->groupBy('variety')->orderBy('product_range', 'asc')->get();
		$viewData['categories'] = Category::where('visible', 1)->get();
		if(isset($viewData['products'])) {
			return View::make('product-category', $viewData);
		} else {
			return App::abort(404, 'Category Doesn\'t Exisit');
		}
		
	}

	public function ProductGet($id, $slug) {

		//Set Session
		Helpers::HasSession();
		Helpers::AddProduct($id);
		
		$viewData['product'] = Product::find($id);
		$viewData['product_sizes'] = Product::select('product_id', DB::raw('CONCAT(pot_size, "  ", state, "  ", form) AS full_variety'))->where('variety', $viewData['product']->variety)->where('recomended', 1)->orderBy('product_id')->lists('full_variety', 'product_id')->all();
		$viewData['categories'] = Category::where('visible', 1)->get();

		SEOMeta::setTitle(''.ucwords($viewData['product']->variety).' | Garden Supplies | JFH Horticultural');
		$client = new \GuzzleHttp\Client();
		try {
		  $response = $client->request('GET', 'http://www.jfhhorticultural.com/img/cat/Specification%20Sheets/'.$id.'.pdf');
		  $viewData['data_sheet'] = true;
		} catch (\GuzzleHttp\Exception\ClientException $exception) {
		  $viewData['data_sheet'] = false;
		}

		$viewData['recents'] = $users = DB::table('recenty_viewed')
			->limit(3)
			->where('session_id', Session::get('session_id'))
            ->join('product', 'recenty_viewed.product_id', '=', 'product.product_id')
            ->groupBy('product.product_id')
            ->orderBy('created_at', 'DESC')
            ->get();

        $rating_product = Product::find($id);
        $viewData['rating'] = round($rating_product->getReviews->avg('rating'));

		return View::make('product', $viewData);

	}

	public function ProductUpdatesGet() {
		return Product::find(Input::get('id'));
	}

	public function ProductUpdatesBreakGet() {
		$viewData['product'] = Product::find(Input::get('id'));
		return View::make('break', $viewData);
	}

	public function AddToCart() {

		//Get Product details
		//Work out the item price;
		
		if(empty(Helpers::IsUserLoggedIn())) {
			return Redirect::back()->with('Error', 'You need to be logged in to add products to your basket, please either login or register today.');
		} else {				
			
			if(Input::has('pot_size')) {
				$product = Product::find(Input::get('pot_size'));
			} else {
				$product = Product::find(Input::get('product_id'));
			}

			//Get Price Break Information
			$break_a = $product->break_a;
			$break_b = $product->break_b ?: '1000000';
			$break_c = $product->break_c ?: '1000000';
			$break_d = $product->break_d ?: '1000000';
			$break_e = $product->break_e ?: '1000000';

			//Work out product price
			$qty = Input::get('qty');

			if(empty($break_b)) {

				$price = $product->price_a;

			} else {

				if($qty >= $break_a && $qty < $break_b) {
					$price = $product->price_a;
				}
				if($qty >= $break_b && $qty < $break_c) {
					$price = $product->price_b;
				}
				if($qty >= $break_c && $qty < $break_d) {
					$price = $product->price_c;
				}
				if($qty >= $break_d && $qty < $break_e) {
					$price = $product->price_d;
				}
				if($qty >= $break_e) {
					$price = $product->break_e;
				}
			}

			Cart::add($product->product_id, $product->variety, Input::get('qty'), $price, ['size' => $product->pot_size, 'description' => $product->form]);
			$nice_title = ucwords($product->variety);

			$customer_email = Customer::where('customer_id', Session('customer_id'))->pluck('email');

			if(Session::has('order_id')) {
				$order = Orders::find(Session::get('order_id'));
				$order->dateoforder = Carbon::now();
				$order->payment_gross = cart::subtotal();
				$order->vat_charged = cart::tax();
				$order->save();
			} else {
				$order = new Orders;
				$order->customer_id = Session('customer_id');
				$order->timedate = Carbon::now();
				$order->payment_email = $customer_email;
				$order->order_taken = 0;
				$order->vat_code = 'T1';
				$order->guid = rand();
				$order->dateoforder = Carbon::now();
				$order->payment_type = 'paypal';
				$order->payment_gross = cart::subtotal();
				$order->vat_charged = cart::tax();
				$order->save();

				Session::set('order_id', $order->orders_id);
			}

			return Redirect::back()->with('Added', ''.$nice_title.' added to your basket.');
		}

	}

	public function CreatePayPal() {

		$apiContext = new \PayPal\Rest\ApiContext(
	    new \PayPal\Auth\OAuthTokenCredential(
	        'AZZ9gnA4DLXxO7Xmd8twf5S6SffBGm_9ZES9407HagpZt0iFbuhe-N0jWUjKgps-_PBdUJVI0gti8JOt',     // ClientID
	        'EOpLFGDIuz30dST4DUGJR6nO9pt4oTun1rMhrE2Oa0FX7lygE13MaQ2HjpoZ2B5UJ5DS0QRQyRybGqPP'      // ClientSecret
	    	)
		);

		$payer = new Payer(); 
		$payer->setPaymentMethod("paypal");

		$item1 = new Item(); 
		$item1->setName('Ground Coffee 40 oz') 
		->setCurrency('USD') 
		->setQuantity(1) 
		->setSku("123123") 
		->setPrice(7.5); 

		$item2 = new Item(); 
		$item2->setName('Granola bars') 
		->setCurrency('USD') 
		->setQuantity(5) 
		->setSku("321321") 
		->setPrice(2); 

		$itemList = new ItemList(); 
		$itemList->setItems(array($item1, $item2));

		$details = new Details(); 
		$details->setShipping(1.2) 
		->setTax(1.3) 
		->setSubtotal(17.50);

		$amount = new Amount(); 
		$amount->setCurrency("USD")
		->setTotal(20)
		->setDetails($details);

		$transaction = new Transaction(); 
		$transaction->setAmount($amount) 
		->setItemList($itemList) 
		->setDescription("Payment description") 
		->setInvoiceNumber(uniqid());

		$redirectUrls = new RedirectUrls(); 
		$redirectUrls->setReturnUrl("http://jfh.app/ExecutePayment.php?success=true") 
		->setCancelUrl("http://jfh.app/ExecutePayment.php?success=false");

		$payment = new Payment(); 
		$payment->setIntent("sale") 
		->setPayer($payer) 
		->setRedirectUrls($redirectUrls) 
		->setTransactions(array($transaction));

		$request = clone $payment;

		try { 
			$payment->create($apiContext); 
		} catch (Exception $ex) {
			 ResultPrinter::printError("Created Payment Using PayPal. Please visit the URL to Approve.", "Payment", null, $request, $ex); 
			 exit(1); 
		}

		return $approvalUrl = $payment->getApprovalLink();


	}

	public function ProductComparison(Request $request) {

		$comparison = new ComparisonTable;
		$comparison->session_id = Session::get('session_id');
		$comparison->product_id = $request->product_id;
		$comparison->save();

		return Redirect::back()->with('Comparison', 'Product added to comparison, view it here:');

	}

	public function ViewProductComparison() {

		$viewData['comparisons'] = $users = DB::table('product_comparison')
			->limit(4)
			->where('session_id', Session::get('session_id'))
            ->join('product', 'product_comparison.product_id', '=', 'product.product_id')
            ->groupBy('product.product_id')
            ->orderBy('created_at', 'DESC')
            ->get();

        return View::make('comparison', $viewData);

	}

}