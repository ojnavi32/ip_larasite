<?php

namespace JFH\Http\Controllers;

use JFH\BlogPosts;
use Input, Redirect, View, SEOMeta;
use Illuminate\Http\Request;
use JFH\Http\Requests;

class FrontBlogController extends Controller
{
    public function index() {
		SEOMeta::setTitle('Blog | Garden Supplies Cheshire | JFH horticultural');
        SEOMeta::setDescription('This is my page description');
    	$viewData['posts'] = BlogPosts::all()->sortByDesc('id'); // <-- scott pls no (id -> created_at) thx.
    	return View::make('blog-posts', $viewData);
    }
}
