<?php

namespace JFH\Http\Controllers;

use JFH\Testimonials;
use Input, Redirect;
use Illuminate\Http\Request;
use JFH\Http\Requests;

class TestiController extends Controller
{

	public function index() {

		$viewData['testimonials'] = Testimonials::all()->sortBy('created_at');
		return view('admin.testi.all', $viewData);

	}

	public function create() {

		return view('admin.testi.create');
	}

	public function store() {
			
		//Get Data and Store in DB.

		$testi = new Testimonials;
		$testi->customer_name = Input::get('customer_name');
		$testi->content = Input::get('content');
		$testi->save();

		return Redirect::back()->with('Added', 'New Testimonial added');

	}

	public function show($id) {

		$testi = Testimonials::find($id);
		return $testi;

	}

	public function edit($id) {

		$viewData['testimonial'] = Testimonials::find($id);
		return view('admin.testi.edit', $viewData);

	}

	public function update($id) {

		//Find the current testimonial
		
		$testi = Testimonials::find($id);
		//Get updates.
		$testi->customer_name = Input::get('customer_name');
		$testi->content = Input::get('content');
		$testi->save();

		return Redirect::back()->with('Updated', 'Testimonial updated');

	}

}
