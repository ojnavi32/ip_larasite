<?php

namespace JFH\Http\Controllers;

use JFH\Product, JFH\Category;
use JFH\Helpers, View, Input, Redirect, Cart, DB;
use Illuminate\Http\Request;
use JFH\Http\Requests;

class SearchController extends Controller
{
    public function search(Request $request) {

    	$search = '%'.$request->search.'%';
		$viewData['products'] = Product::where('recomended', 1)->where(function($q) use($search) { 

			$q
			->where('variety', 'LIKE', $search)
			->orWhere('prod_desc', 'LIKE', $search)
			->orWhere('cataloguecode', 'LIKE', $search); 
				
		})->groupBy('variety')->get();

		$viewData['categories'] = Category::where('visible', 1)->get();
		return View::make('search', $viewData);
    
    }

    public function searchquery(Request $request) {

    	$search = '%'.$request->search.'%';
		$viewData['products'] = Product::where('recomended', 1)->where(function($q) use($search) { 

			$q
			->where('variety', 'LIKE', $search)
			->orWhere('prod_desc', 'LIKE', $search)
			->orWhere('cataloguecode', 'LIKE', $search); 
				
		})->groupBy('variety')->get();

		$viewData['categories'] = Category::where('visible', 1)->get();
		return View::make('search', $viewData);
    
    }
}
