<?php

namespace JFH\Http\Controllers\Customer;

use JFH\Product, JFH\Category, JFH\CustomerProspect, JFH\Customer, JFH\Orders, JFH\OrderItems, JFH\CustomerAddress, JFH\ProductReview;
use JFH\Helpers, View, Input, Redirect, Cart, Session, SEOMeta, Mail, PDF;
use Illuminate\Http\Request;
use JFH\Http\Requests;
use JFH\Http\Controllers\Controller;

class CustomerController extends Controller
{
	public function index() {
		SEOMeta::setTitle('Your Account | Garden Supplies Cheshire | JFH horticultural');
        SEOMeta::setDescription('This is my page description');
		return View::make('customer.register-login');
	}

	public function register() {

		//Register the new customer as a prospect.
		
		//Check if customer is already registered as a prospect.
		$customer_check = CustomerProspect::where('email', Input::get('email'))->first();

		if(empty($customer_check)) {

			$customer = new CustomerProspect;
			$customer->fname = Input::get('fname');
			$customer->email = Input::get('email');
			$customer->password = Input::get('password');
			$customer->company = Input::get('company_name');
			$customer->phone_mobile = Input::get('mobile_number');
			$customer->phone_office = Input::get('phone_number');
			$customer->address_l1 = Input::get('address_1');
			$customer->address_l2 = Input::get('address_2');
			$customer->address_l3 = Input::get('address_3');
			$customer->postcode = Input::get('post_code');
			$customer->save();

			$emailData['fname'] = Input::get('fname');
	        $emailData['address_l1'] = Input::get('address_1');
	        $emailData['address_l2'] = Input::get('address_2');
	        $emailData['address_l3'] = Input::get('address_3');
	        $emailData['postcode'] = Input::get('post_code');
	        $emailData['email'] = Input::get('email');
	        $emailData['company'] = Input::get('company_name');
	        $emailData['password'] = Input::get('password');
	        $emailData['phone_mobile'] = Input::get('mobile_number');
	        $emailData['phone_office'] = Input::get('phone_number');
	        $emailData['title'] = 'New Registration';

			Mail::send('email.newaccount', $emailData, function ($message)
	        {
	            $message->from('website@firstinternet.co.uk', 'JFH Website');
	            $message->to('katrina@jfhhorticultural.com');
	            //$message->to('scott@firstinternet.co.uk');
	        });

			return Redirect::route('customer-register-thanks')->with('Good', 'We\'ve recieved your application, please allow us up to 24hours to review it.');

		} else {
			return Redirect::back()->with('Error', 'You have already registered as a prospect, please allow us up to 24hours to review your account.');
		}

	}

	public function quickRegistration(Request $request) {
		$customer = new CustomerProspect;
		$customer->fname = $request->name;
		$customer->email = $request->emailAddress;
		$customer->password = $request->password;
		$customer->save();

		$emailData['fname'] = $request->name;
        $emailData['email'] = $request->emailAddress;
        $emailData['password'] = $request->password;
        $emailData['title'] = 'New Registration';
        $emailData['body'] = 'This user registered using the quick registration on the homepage, we don\'t have any address or company information because of this. Please email them';

		Mail::send('email.newaccount', $emailData, function ($message)
        {
            $message->from('website@firstinternet.co.uk', 'JFH Website - Quick Contact');
            $message->to('katrina@jfhhorticultural.com');
            //$message->to('scott@firstinternet.co.uk');
        });

		return Redirect::route('customer-register-thanks')->with('Good', 'We\'ve recieved your application, please allow us up to 24hours to review it.');
	}

	public function thanks() {
		return View::make('customer.register-login')->with('Good', 'We\'ve recieved your application, please allow us up to 24hours to review it.');
	}

	public function Login() {

		// Try and log customer in.
		$customer = Customer::where('email', Input::get('username'))->where('password', Input::get('password'))->first();

		if(empty($customer)) {
			return Redirect::back()->with('Error', 'The username or password you used was not recognised, please check and try again.');
		} else {
			//Set Session.
			Session::set('loggedin', '1');
			Session::set('customer_id', $customer->customer_id);
			//return Redirect::route('main-home');
			return redirect('product/categories');
		}

	}

	public function logout() {

		//kill all sessions
		
		Session::clear();
		return Redirect::route('main-home');

	}

	public function home() {

		// Check if customer is logged in.
		
		if(empty(Helpers::IsUserLoggedIn())) {
			return Redirect::back()->with('Error', 'Sorry, you\'re not logged in, please try again');
		} else {
			return View::make('customer.home');
		}

	}

	public function orders() {
		if(empty(Helpers::IsUserLoggedIn())) {
			return Redirect::back()->with('Error', 'Sorry, you\'re not logged in, please try again');
		} else {

			// Find Orders
			
			$viewData['orders'] = Orders::where('customer_id', Session('customer_id'))->get();
			//return $viewData['orders'];
			return View::make('customer.orders.index', $viewData);
		}
	}

	public function OrdersView($id) {

		$account_order = Orders::find($id);
		if($account_order->customer_id == Session::get('customer_id')) {
			$viewData['order'] = Orders::find($id);
			$viewData['order_id'] = $id;
			$viewData['orderitems'] = OrderItems::where('orders_id', $id)->get();
			$viewData['customer'] = Customer::find(Session::get('customer_id'));
			return View::make('customer.orders.view', $viewData);
		} else {
			return Redirect::route('customer-home');
		}
	}

	public function viewPdf($id) {

		$account_order = Orders::find($id);
		if($account_order->customer_id == Session::get('customer_id')) {
			$viewData['order'] = Orders::find($id);
			$viewData['order_id'] = $id;
			$viewData['orderitems'] = OrderItems::where('orders_id', $id)->get();
			$viewData['customer'] = Customer::find(Session::get('customer_id'));
			$pdf = PDF::loadView('customer.orders.view', $viewData);
			return $pdf->download('invoice.pdf');
		} else {
			return Redirect::route('customer-home');
		}

	} 

	public function GetCreditApplication() {
		return View::make('customer.credit-form');		
	}

	public function SubmitCreditApplication(Request $request) {

	    $this->validate($request, [
	        'NameOfBusiness' => 'required',
	        'address' => 'required',
			'address1' => 'required',
			'town' => 'required',
			'postcode' => 'required',
			'address' => 'required',
			'business_type' => 'required',
			'companyregno' => 'required',
			'extra_company_info' => 'required',
			'payment_method' => 'required',
			'credit_limit' => 'required',
			'trade_references' => 'required',
			'2ndtrade_references' => 'required',
			'bank_name' => 'required',
			'bank_address' => 'required',
			'sort_code' => 'required',
			'account_number' => 'required',
			'references' => 'required',
			'checkbox' => 'required'
	    ]);
	
		$emailData['NameOfBusiness'] = $request->input('NameOfBusiness');
        $emailData['address'] = $request->input('address');
        $emailData['address1'] = $request->input('address1');
		$emailData['town'] = $request->input('town');
        $emailData['postcode'] = $request->input('postcode');
        $emailData['business_type'] = $request->input('business_type');
		$emailData['companyregno'] = $request->input('companyregno');
        $emailData['extra_company_info'] = $request->input('extra_company_info');
        $emailData['payment_method'] = $request->input('payment_method');
		$emailData['credit_limit'] = $request->input('credit_limit');
        $emailData['trade_references'] = $request->input('trade_references');
        $emailData['2ndtrade_references'] = $request->input('2ndtrade_references');        
        $emailData['bank_name'] = $request->input('bank_name');
        $emailData['bank_address'] = $request->input('bank_address');
        $emailData['sort_code'] = $request->input('sort_code');
        $emailData['account_number'] = $request->input('account_number');
        $emailData['references'] = $request->input('references');
        $emailData['title'] = 'Credit Application';

        Mail::send('email.credit', $emailData, function ($message)
        {
            $message->from('website@firstinternet.co.uk', 'JFH Website');
            $message->to('katrina@jfhhorticultural.com');
            $message->bcc('scott@firstinternet.co.uk');
        });

        return Redirect::back();
	}

	public function trackOrder() {
		return View::make('customer.customer.track');
	}

	public function trackOrderPallet(Request $request) {
		$viewData['con'] = $request->connumber;
		$viewData['postcode'] = $request->postcode;
		return View::make('customer.customer.track-pallet', $viewData);
	}

	public function viewAddresses() {
		$viewData['addresses'] = Customer::find(Session::get('customer_id'))->getAddresses;
		return View::make('customer.customer.address-view', $viewData);
	}

	public function addAddresses() {
		return View::make('customer.customer.address-add');
	}

	public function getAddress() {
		$client = new \petelawrence\getaddress\GetAddressClient('C9vsiZALN0m-_3HfEr_ikg6663');
	    $result = $client->lookup('OL115XA');
	    $addresses = $result->getAddresses();
	    return $addresses;
	}

	public function storeAddress(Request $request) {
		$customeraddress = new CustomerAddress;
		$customeraddress->customer_id = Session::get('customer_id');
		$customeraddress->address1 = $request->line1;
		$customeraddress->address2 = $request->line2;
		$customeraddress->address_town = $request->town;
		$customeraddress->address_postcode = $request->postcode;
		$customeraddress->save();

		return redirect()->route('view-address');

	}

	public function viewRating($id) {
		$viewData['product_id'] = $id;
		return View::make('customer.customer.add-review', $viewData);
	}

	public function addRating(Request $request) {
		
		$review = new ProductReview;
		$data = array(
			'customer_id' => Session::get('customer_id'), 
			'product_id' => $request->product_id, 
			'rating' => $request->rating, 
			'review' => $request->review, 
		);
		$review->fill($data);
		$review->save();

		return Redirect::route('get-product', array('id' => $request->product_id, 'slug' => 'review'));

	}
}