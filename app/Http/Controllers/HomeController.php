<?php

namespace JFH\Http\Controllers;

use JFH\Category, JFH\BlogPosts;
use SEOMeta;
use JFH\Testimonials;
use JFH\Helpers, View;
use JFH\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index() {

    	SEOMeta::setTitle('Home | Garden Supplies Cheshire | JFH horticultural');
        SEOMeta::setDescription('This is my page description');
        $viewData['posts'] = BlogPosts::all()->take(3);
    	$viewData['categories'] = Category::where('visible', 1)->get();
    	return View::make('index', $viewData);
    }

    public function testi() {
    	SEOMeta::setTitle('Testimonials | Garden Supplies Cheshire | JFH horticultural');
        SEOMeta::setDescription('This is my page description');
    	$viewData['testimonials'] = Testimonials::all();
    	return View::make('testi', $viewData);
    }
}