<?php

namespace JFH\Http\Controllers;

use Illuminate\Http\Request;
use Mail, Redirect, View, SEOMeta;

use JFH\Http\Requests;

class MiscController extends Controller {


	public function ContactPost(Request $request) {

        $emailData['email'] = $request->input('title');
        $emailData['phone'] = $request->input('phone');
        $emailData['name'] = $request->input('name');
        $emailData['title'] = 'Call Back Request';

        Mail::send('email.callback', $emailData, function ($message)
        {
            $message->from('website@firstinternet.co.uk', 'JFH Website');
            $message->to('katrina@jfhhorticultural.com');
            $message->bcc('scott@firstinternet.co.uk');
        });

        return Redirect::back();

	}

    public function ContactFromPost(Request $request) {

        $emailData['email'] = $request->input('title');
        $emailData['phone'] = $request->input('phone');
        $emailData['name'] = $request->input('name');
        $emailData['title'] = 'Contact Form';

        Mail::send('email.callback', $emailData, function ($message)
        {
            $message->from('website@firstinternet.co.uk', 'JFH Website');
            $message->to('katrina@jfhhorticultural.com');
        });

        return Redirect::back();

    }

    public function ViewTerms() {
        SEOMeta::setTitle('Terms and Conditions | Garden Supplies Cheshire | JFH horticultural');
        return View::make('terms');
    }

    public function ViewPrivacy() {
        SEOMeta::setTitle('Privacy Policy | Garden Supplies Cheshire | JFH horticultural');
        return View::make('privacy');
    }

    public function returnAddress(Request $request) {

        $client = new \petelawrence\getaddress\GetAddressClient('C9vsiZALN0m-_3HfEr_ikg6663');
        $result = $client->lookup($request->postcode, $request->house_number);
        $address = $result->getAddresses()[0];
        return json_encode($address);

    }

}
