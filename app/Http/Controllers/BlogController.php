<?php

namespace JFH\Http\Controllers;

use JFH\BlogPosts;
use Input, Redirect;
use Illuminate\Http\Request;
use JFH\Http\Requests;

class BlogController extends Controller
{
    
	public function index() {

		$viewData['posts'] = BlogPosts::all()->sortBy('created_at');
		return view('admin.blog.all', $viewData);

	}

	public function create() {

		return view('admin.blog.create');
	}

	public function store(Request $request) {
			
		//Get Data and Store in DB.

		$blog = new BlogPosts;
		$blog->title = Input::get('title');

		//Get file and Save
    	$imageName = $request->file('image')->getClientOriginalName();
        $request->file('image')->getClientOriginalExtension();

	    $request->file('image')->move(
	        base_path() . '/public/images/blog/', $imageName
	    );

	    $blog->filename = $imageName;
		$blog->publish_date = date('Y-m-d', strtotime(Input::get('publish_date')));//Input::get('publish_date');
		$blog->meta_title = Input::get('meta_title');
		$blog->meta_desc = Input::get('meta_description');
		$blog->content = Input::get('content');
		$slug = toAscii(Input::get('title'));
		$blog->slug = $slug;
		
		$blog->save();

		return Redirect::back()->with('Added', 'New blog post added');

	}

	public function show($id) {

		$post = BlogPosts::find($id);
		return $post;

	}

	public function edit($id) {

		$viewData['post'] = BlogPosts::find($id);
		return view('admin.blog.edit', $viewData);

	}

	public function update(Request $request, $id) {

		//Find the current blog
		
		$blog = BlogPosts::find($id);
		//Get updates.
		$blog->title = Input::get('title');
		$blog->meta_title = Input::get('meta_title');
		$blog->publish_date = date('Y-m-d', strtotime(Input::get('publish_date')));
		//Get file and Save
		if(Input::has('delete')) {
	    	$imageName = $request->file('image_testing')->getClientOriginalName();
	        $request->file('image_testing')->getClientOriginalExtension();

		    $request->file('image_testing')->move(
		        base_path() . '/public/images/blog/', $imageName
		    );

		    $blog->filename = $imageName;
		}

		$blog->meta_desc = Input::get('meta_description');
		$blog->content = Input::get('content');
		$blog->save();

		return Redirect::back()->with('Updated', 'Post updated');

	}

}


setlocale(LC_ALL, 'en_US.UTF8');
function toAscii($str, $replace=array(), $delimiter='-') {
	if( !empty($replace) ) {
		$str = str_replace((array)$replace, ' ', $str);
	}

	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	return $clean;
}
