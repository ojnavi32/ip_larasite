<?php

namespace JFH\Http\Controllers\admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use \View, JFH\Settings, Input, Redirect, JFH\Category;
use JFH\Http\Requests;
use JFH\Http\Controllers\Controller;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return View::make('admin.category.all');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        $category->fill($request->all());
        $category->save();
        return 'Added';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $viewData['category'] = Category::where('product_category_id',$id)->first();
        return View::make('admin.category.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $category->update($request->all());
        $category->save();

        return Redirect::back()->with('Updated', 'Product information has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getAllCategories() {
                
        $categories = Category::select(['product_category_id','title','img_src','visible','position']);
        return Datatables::of($categories)
        ->addColumn('action', function ($categories) {
                return '<a href="/admin/categories/'.$categories->product_category_id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
        ->editColumn('img_src', '<img src="/images/assets/{{$img_src}}">')
        ->make();

    }
}
