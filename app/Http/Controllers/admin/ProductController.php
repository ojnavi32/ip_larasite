<?php

namespace JFH\Http\Controllers\admin;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use \View, JFH\Settings, Input, Redirect, JFH\Product;
use JFH\Http\Requests;
use JFH\Http\Controllers\Controller;

class ProductController extends Controller
{

	public function index() {

		return View::make('admin.product.all');

	}

	public function edit($id) {

		$viewData['product'] = Product::find($id);
		return View::make('admin.product.edit', $viewData);

	}

	public function update(Request $request, $id) {
		$product = Product::find($id);
		$product->update($request->all());
        $product->save();

        return Redirect::back()->with('Updated', 'Product information has been updated.');
	}

	public function getAllProducts() {

		$products = Product::select(['product_id','variety','state','product_range','stock','price_a']);
		return Datatables::of($products)
        ->addColumn('action', function ($products) {
                return '<a href="/admin/products/'.$products->product_id.'/edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
		->make();

	}

}
