<?php

namespace JFH\Http\Controllers\admin;

use Illuminate\Http\Request;
use View, JFH\Banners, Redirect;
use JFH\Http\Requests;
use JFH\Http\Controllers\Controller;

class BannerController extends Controller
{
    public function index() {
    	$viewData['banners'] = Banners::all();
    	return View::make('admin.banners.index', $viewData);
    }

    public function create() {
    	return View::make('admin.banners.create');
    }

    public function store(Request $request) {
    	//Save Banners
    	$banner = New Banners;
    	$banner->name = $request->input('name');
    	$banner->location = $request->input('location');

    	//Get file and Save
    	$imageName = $request->file('image')->getClientOriginalName();
        $request->file('image')->getClientOriginalExtension();

	    $request->file('image')->move(
	        base_path() . '/public/images/banners/', $imageName
	    );

	    $banner->file_location = $imageName;
	    $banner->save();

	    return Redirect::back()->with('Added', 'Banner Added');

    }

    public function edit($id) {

        $viewData['banner'] = Banners::find($id);
        return View::make('admin.banners.edit', $viewData);

    }

    public function update(Request $request, $id) {

        $banner = Banners::find($id);
        $current_image = $banner->file_location;

        $banner->name = $request->input('name');
        $banner->location = $request->input('location');

        if($request->has('delete_image')) {
            $imageName = $request->file('image')->getClientOriginalName();
            $request->file('image')->getClientOriginalExtension();

            $request->file('image')->move(
                base_path() . '/public/images/banners/', $imageName
            );

            $banner->file_location = $imageName;
        }

        $banner->save();

        return Redirect::back()->with('Updated', 'Banner Updated');
    }
}
