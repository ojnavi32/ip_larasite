<?php

namespace JFH\Http\Controllers\admin;

use Illuminate\Http\Request;
use \View, JFH\Settings, Input, Redirect;
use JFH\Http\Requests;
use JFH\Http\Controllers\Controller;

class SettingsController extends Controller
{
    
	 public function index() {

	 	
	 	$viewData['settings'] = Settings::first();
	 	return View::make('admin.settings.index', $viewData);


	 }

	 public function store() {

	 	//get details from form
	 	$settings = New Settings;
	 	$settings->telephone_number = Input::get('telephone_number');
	 	$settings->fax_number = Input::get('telephone_number');
	 	$settings->email_address = Input::get('telephone_number');
	 	$settings->google_ua = Input::get('google_ua');
	 	$settings->google_wmt = Input::get('google_wmt');	 	
	 	$settings->facebook = Input::get('facebook');
	 	$settings->twitter = Input::get('twitter');
	 	$settings->linkedin = Input::get('linkedin');
	 	$settings->google_plus = Input::get('google_plus');
	 	$settings->about_us = Input::get('about_us');
	 	$settings->save();

	 	return Redirect::back()->with('Added', "Settings Added");

	 }

	 public function update($id) {

	 	//Find details
	 	$settings = Settings::find($id);
	 	$settings->telephone_number = Input::get('telephone_number');
	 	$settings->fax_number = Input::get('telephone_number');
	 	$settings->email_address = Input::get('telephone_number');
	 	$settings->google_ua = Input::get('google_ua');
	 	$settings->google_wmt = Input::get('google_wmt');
	 	$settings->facebook = Input::get('facebook');
	 	$settings->twitter = Input::get('twitter');
	 	$settings->linkedin = Input::get('linkedin');
	 	$settings->google_plus = Input::get('google_plus');
	 	$settings->about_us = Input::get('about_us');
	 	$settings->save();

	 	return Redirect::back()->with('Added', "Settings Saved");

	 }

}
