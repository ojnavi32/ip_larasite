<?php

namespace JFH\Http\Controllers\admin;

use Illuminate\Http\Request;
use JFH\Team, Redirect, Input, View;
use JFH\Http\Requests;
use JFH\Http\Controllers\Controller;

class TeamController extends Controller
{
    public function index() {

    	$viewData['teams'] = Team::all();
    	return View::make('admin.team.index', $viewData);

    }

    public function create() {
    	return View::make('admin.team.create');
    }

    public function store(Request $request) {

    	//Save Banners
    	$team = New Team;
    	$team->name = $request->input('name');
    	$team->job_title = $request->input('job_title');
    	$team->about = $request->input('about');
    	$team->telephone_number = $request->input('telephone_number');
    	$team->email_address = $request->input('email_address');

    	//Get file and Save
    	$imageName = $request->file('image')->getClientOriginalName();
        $request->file('image')->getClientOriginalExtension();

	    $request->file('image')->move(
	        base_path() . '/public/images/team/', $imageName
	    );

	    $team->image = $imageName;
	    $team->save();

	    return Redirect::back()->with('Added', 'Banner Added');

    }

    public function edit($id) {
        $viewData['team'] = Team::find($id);
        return View::make('admin.team.edit', $viewData);        
    }

    public function update(Request $request, $id) {

        //Save Banners
        $team = Team::find($id);
        $team->name = $request->input('name');
        $team->job_title = $request->input('job_title');
        $team->about = $request->input('about');
        $team->telephone_number = $request->input('telephone_number');
        $team->email_address = $request->input('email_address');

        //Get file and Save
        if($request->has('delete')) {
            $imageName = $request->file('image')->getClientOriginalName();
            $request->file('image')->getClientOriginalExtension();

            $request->file('image')->move(
                base_path() . '/public/images/team/', $imageName
            );

            $team->image = $imageName;
        }
        $team->save();

        return Redirect::back()->with('Updated', 'Team Member Updated');

    }
}
