<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $table = 'orders';
    protected $primaryKey = 'orders_id';
    public $timestamps = false;

    public function OrderItems() {

    	return $this->hasMany('JFH\OrderItems', 'orders_id', 'orders_id');
    }

    public function OrderState(){
    	return $this->hasOne('JFH\OrderState', 'state_id' , 'order_taken');
    }
}
