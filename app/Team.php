<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $table = 'team';
}
