<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;

class ComparisonTable extends Model
{
    protected $table = 'product_comparison';
    protected $dates = ['deleted_at'];
}
