<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;

class OrderItems extends Model
{
    protected $table = 'orddet';
    public $timestamps = false;
}
