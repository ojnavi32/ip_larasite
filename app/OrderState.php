<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;

class OrderState extends Model
{
    protected $table = 'order_state';
    protected $primaryKey = 'state_id';
    public $timestamps = false;
}
