<?php

namespace JFH;
Use Session;
Use JFH\RecentlyViewed;
use JFH\Settings;
use JFH\Customer;

class Helpers {

	public static function getSettings() {
		return Settings::find(1);
	}

	public static function IsUserLoggedIn() {
		if(session('loggedin')) {
			return true;
		} else {
			return false;
		}
	}

	public static function HasSession() {

		//Check If Session Is already active
		if(Session::has('session_id')) {
			return Session::get('session_id');
		} else {
			Session::set('session_id', rand());
			return Session::get('session_id');
		}
	}

	public static function AddProduct($id) {
		$viewed = new RecentlyViewed;
		$viewed->product_id = $id;
		$viewed->session_id = Session::get('session_id');
		$viewed->save();
		return $viewed;
	}

	public static function GetName() {
		if(session('loggedin')) {
			return $customer_name = Customer::where('customer_id', Session::get('customer_id'))->pluck('fname')->first();
		} else {
			return '';
		}
	}

}