<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class CustomerProspect extends Authenticatable
{
    protected $table = 'customer_prospect';
    protected $guard = "customers";
    public $timestamps = false;
}
