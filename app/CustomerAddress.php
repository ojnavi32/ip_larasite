<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    protected $table = 'customer_address';
    public $fillable = ['customer_id','address1','address2','address_town','address_postcode'];
}