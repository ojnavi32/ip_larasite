<?php

namespace JFH;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $primaryKey = 'product_id';
    public $timestamps = false;

    protected $fillable = [
		'product_id',
		'variety',
		'pot_size',
		'product_range',
		'state',
		'stock',
		'tray_quantity',
		'catagory',
		'availability',
		'recomended',
		'cataloguecode',
		'form',
		'price_a',
		'price_b',
		'price_c',
		'price_d',
		'price_e' ,
		'break_a',
		'break_b',
		'break_c',
		'break_d' ,
		'break_e',
		'break_a_desc',
		'break_b_desc',
		'break_c_desc',
		'break_d_desc',
		'break_e_desc',
		'vatcode',
		'uop',
		'barcode',
		'department_id',
		'prod_desc',
		'priceper',
    ];

    public function metaDescription() {
    	return $this->hasOne('JFH\productMetaDescriptions', 'product_id', 'product_id');
    }

    public function getReviews() {
    	return $this->hasMany('JFH\ProductReview', 'product_id', 'product_id');
    }
}
